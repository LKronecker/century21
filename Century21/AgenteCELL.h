//
//  AgenteCELL.h
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-11-07.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgenteCELL : UITableViewCell{
    UILabel *tittleLabel;
    UILabel *subTittle;
    UIImageView *cellImageView;
    
}
@property (nonatomic, retain) IBOutlet UILabel *tittleLabel;
@property (nonatomic, retain) IBOutlet UILabel *subTittle;
@property (nonatomic, retain) IBOutlet   UIImageView *cellImageView;



@end
