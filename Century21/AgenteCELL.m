//
//  AgenteCELL.m
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-11-07.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "AgenteCELL.h"

@interface AgenteCELL ()

@end

@implementation AgenteCELL
@synthesize cellImageView, tittleLabel, subTittle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
