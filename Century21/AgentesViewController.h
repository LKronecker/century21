//
//  AgentesViewController.h
//  Century21
//
//  Created by Alan MB on 09/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "DetallesAgente.h"
@class DetallesAgente;

@interface AgentesViewController : UITableViewController{
    NSMutableArray *agentes;
    DetallesAgente *detA;
    
}
@property (nonatomic, retain) NSMutableArray *agentes;
@property (nonatomic, retain)    DetallesAgente *detA;

-(IBAction)gotoBack:(id)sender;

@end

