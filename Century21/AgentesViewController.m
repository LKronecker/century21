//
//  AgentesViewController.m
//  Century21
//
//  Created by Alan MB on 09/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "AgentesViewController.h"
#import "AgenteCELL.h"

@interface AgentesViewController ()

@end

@implementation AgentesViewController
@synthesize agentes, detA;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{

}

- (void)viewDidLoad
{
    ////Request
    NSURL *url = [[NSURL alloc]initWithString:@"http://century21.elolabs.com:8888/update_agentes"];
    
 //   NSLog(@"%@, %@, %@", url, la, lo);
    
    NSData* data = [NSData dataWithContentsOfURL:
                    url];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data waitUntilDone:YES];

    ////
   // NSArray *ag = [[NSArray alloc]initWithObjects:@"Trueba de torres",@"Lomax + Covarrubias", @"Mariscal", @"Doniz & Asociados", nil];
   // self.agentes = ag;
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    // [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    ///////
    [super viewDidLoad];
    }

- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
   // NSArray* latestLoans = [json objectForKey:@"propiedades"]; //2
    NSArray* keys = [[json objectForKey:@"agentes"] allKeys];
  
    
       self.agentes = [[NSMutableArray alloc] initWithCapacity:[keys count]];
  
    
    for (int i =0; i < [keys count]; i++) {
        
        
        NSString *keyProp = [NSString stringWithFormat:@"%@", [keys objectAtIndex:i]];
        
        NSDictionary* propiedad = [[json objectForKey:@"agentes"] objectForKey:keyProp];
        
        // 2) Get the funded amount and loan amount
        NSString* nameST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"nombre"] ];
       
        
        [self.agentes insertObject:nameST atIndex:i];
     
   
        //   NSLog(@"name: %@", nameST);
        
        
    }
    
    NSLog(@"%i", [keys count]);
   }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)gotoBack:(id)sender
{
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"Ultimo-header-pagina-principal.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [agentes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AgenteCELL *cell;
    static NSString *CellIdentifier = @"Cell";
    
    cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    NSUInteger row = [indexPath row];
    NSString *rowTitle = [self.agentes objectAtIndex:row];
    cell.tittleLabel.text = rowTitle;
    
    NSString *name = [[NSString alloc]initWithFormat:@"Imagen-no-disponible.png"];
    UIImage *image = [UIImage imageNamed:name];
    cell.cellImageView.image = image;
    
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetallesAgente *mp = [self.storyboard instantiateViewControllerWithIdentifier:@"DetAG"];
     NSUInteger row = [indexPath row];
    NSString *imST = [[NSString alloc]initWithFormat:@"%i", row];
    mp.agente = imST;
    mp.procedence = @"0";
    self.detA = mp;
    [self.navigationController pushViewController:mp animated:YES];
    
  }

@end
