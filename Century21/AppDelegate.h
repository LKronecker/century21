//
//  AppDelegate.h
//  Century21
//
//  Created by Alan MB on 09/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//-(UIStoryboard*) getStoryboard;

@end
