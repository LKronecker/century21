//
//  BusquedaAvanzada.h
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-11-18.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusquedaAvanzada : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>{
    UIView *searchView;
    UIPickerView * estadosPicker;
    NSArray *estadosSearch;
    NSArray *ciudadesSearch;
    NSArray *coloniasSearch;
    
    
    bool searchControl;
    bool actionSearchBool;

    UIImageView *notifCiudad;
    UILabel *searchLabel;
    
    UIActivityIndicatorView *activityIndicator;
}

@property (nonatomic, retain) IBOutlet   UIPickerView * estadosPicker;
@property (nonatomic, retain) NSArray *estadosSearch;
@property (nonatomic, retain) NSArray *ciudadesSearch;
@property (nonatomic, retain) NSArray *coloniasSearch;
@property (nonatomic, retain) IBOutlet  UIView *searchView;

@property  (strong,nonatomic) NSString *estado;
@property  (strong,nonatomic) NSString *ciudad;
@property  (strong,nonatomic) NSString *colonia;

@property (nonatomic, retain) IBOutlet    UIImageView *notifCiudad;
@property (nonatomic, retain) IBOutlet  UILabel *searchLabel;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)exitAction:(id)sender;
-(IBAction)searchAct;

@end
