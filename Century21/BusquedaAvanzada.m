//
//  BusquedaAvanzada.m
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-11-18.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "BusquedaAvanzada.h"
#import "GPSMapViewController.h"

@interface BusquedaAvanzada ()

@end

@implementation BusquedaAvanzada
@synthesize estadosPicker, estadosSearch, searchView, ciudadesSearch, coloniasSearch, notifCiudad,searchLabel, activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
    
    
     [self.activityIndicator startAnimating];
    [self JSONrequest];
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
   
	// Do any additional setup after loading the view.
    
    searchControl = 0;
    actionSearchBool = 0;
    
    self.searchLabel.text = self.estado;
    
    
    
    NSLog(@"%@", self.estado);
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"Fondo-pagina-principal.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ////
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    //  [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)exitAction:(id)sender {
    [self dismissViewControllerAnimated:YES  completion:nil];
}

-(IBAction)searchAct{
    if (actionSearchBool == 0) {
        
    
    searchControl = 1;
    NSInteger row;
    row = [self.estadosPicker selectedRowInComponent:0];
    
    NSString *ciud= [NSString stringWithFormat:@"%@",[self.ciudadesSearch objectAtIndex:row] ];
    self.ciudad = ciud;
    
    [self JSONrequest];
    
    NSLog(@"%@, ", self.ciudad);
        actionSearchBool = 1;
        
        UIImage *colonia = [UIImage imageNamed:@"Escoge-tu-colonia.png"];
        
        [self.notifCiudad setImage:colonia];
        
         self.searchLabel.text =[NSString stringWithFormat:@"%@, %@", self.estado, self.ciudad];
    }else{
        NSInteger row;
        row = [self.estadosPicker selectedRowInComponent:0];
        
        NSString *col= [NSString stringWithFormat:@"%@",[self.coloniasSearch objectAtIndex:row] ];
        self.colonia = col;
        
        NSLog(@"%@; %@; %@", self.estado, self.ciudad, self.colonia);
        
        NSString *searchResSTR = [NSString stringWithFormat:@"http://century21.elolabs.com:8888/search?q=&estado=%@&ciudad=%@&colonia=%@", self.estado, self.ciudad, self.colonia];
        
        searchResSTR = [searchResSTR stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        
        NSLog(@"%@", searchResSTR);
        
        GPSMapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"Mapas"];
        map.MapString = @"3";
        map.searchURL = searchResSTR;
        map.estadoST =self.estado;
        map.ciudadST = self.ciudad;
        map.coloniaST = self.colonia;
    
        [self.navigationController pushViewController:map animated:YES];
        
        
        UIGraphicsBeginImageContext (CGSizeMake(320, 44));
        [[UIImage imageNamed:@"header-2_final.png"] drawInRect:self.navigationController.navigationBar.bounds];
        UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];


    }
    
      
    
}


- (void)JSONrequest {
    
    
    [self.activityIndicator startAnimating];
    
    //    if([MapString isEqualToString:@"0"]){
    
    NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/update_colonias"]];
    
    
    
    //     NSLog(@"%@, %@, %@", url, la, lo);
    
    NSData* data = [NSData dataWithContentsOfURL:
                    url];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data waitUntilDone:YES];
    
    //   NSLog(@"%@", data);
    
    
}

- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    // NSData *jsonData = [NSData data];
    
    //  NSString* dataStr  = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    //  NSData *jsonData = [NSData dataWithContentsOfFile:dataStr];
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSDictionary* EstadosDict = [json objectForKey:@"estados"]; //2
    NSArray* estadosA = [[json objectForKey:@"estados"] allKeys];
    
    self.estadosSearch = estadosA;
    
    NSDictionary* CiudadesDict = [[EstadosDict objectForKey:self.estado] objectForKey:@"ciudades"] ;
    NSArray* ciudadesA =  [[[EstadosDict objectForKey:self.estado] objectForKey:@"ciudades"] allKeys] ;
    
    self.ciudadesSearch = ciudadesA;
    
   // NSDictionary* ColoniasDict = [[CiudadesDict objectForKey:self.ciudad] objectForKey:@"colonias"] ;
      NSArray* coloniasA = [[[CiudadesDict objectForKey:self.ciudad] objectForKey:@"colonias"] allObjects];
   // NSArray *array = [NSArray array];
    self.coloniasSearch = coloniasA;
    
    
    NSLog(@"ESTADOS:%@, CIUDADES:%@, COLONIAS:%@",estadosA, ciudadesA, coloniasA); //3
    
    [self.estadosPicker reloadAllComponents];
    [self.estadosPicker reloadInputViews];
    
    [self.activityIndicator stopAnimating];


}

-(IBAction)gotoBack:(id)sender
{
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"header-pagina-principal_final.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	if (searchControl == 0) {
    return [self.ciudadesSearch count];
    }else{
    return [self.coloniasSearch count];
    }
}
#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (searchControl == 0) {
    return [self.ciudadesSearch objectAtIndex:row];
    }else{
        return [self.coloniasSearch objectAtIndex:row];
    }
    
}


@end
