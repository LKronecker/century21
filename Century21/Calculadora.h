//
//  Calculadora.h
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-10-28.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Calculadora : UIViewController{

    UITextField *anticipo;
    UITextField *anosCredito;
    UITextField *precioVenta;
    UITextField *interes;
    
    UILabel *result;
}


@property (nonatomic, retain) IBOutlet UITextField *anticipo;
@property (nonatomic, retain) IBOutlet UITextField *anosCredito;
@property (nonatomic, retain) IBOutlet UITextField *precioVenta;
@property (nonatomic, retain) IBOutlet UITextField *interes;
 @property (nonatomic, retain) IBOutlet   UILabel *result;

-(IBAction)gotoBack:(id)sender;
-(IBAction)calculate:(id)sender;
- (IBAction) tapBackground: (id) sender;

@end
