//
//  Calculadora.m
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-10-28.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "Calculadora.h"

@interface Calculadora ()

@end

@implementation Calculadora
@synthesize anosCredito, anticipo, precioVenta, interes, result;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"Fondo-pagina-principal.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];

   
    //////
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    //  [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;

}

-(IBAction)calculate:(id)sender{
    
  /*  V = PrecioVenta      (RawInput)
    A = Anticipo              (RawInput)
    C = AñosCredito      (RawInput)
    I = interés                  (RawInput)
    */
    
  //  M = (V-A)*I*(30/365)+((V-A) / (C*12))
    
    float anos = [self.anosCredito.text floatValue];
    float anti = [self.anticipo.text floatValue];
    float pre = [self.precioVenta.text floatValue];
    float inte = [self.interes.text floatValue];
    
    float total =  (((pre - anti) * (inte*30/365) + (pre - anti)) / (anos*12));
   // float result2 = (pre-anti) / (anos*12);
    
   // float roundTOTAL = [self round:total toSignificantFigures:3];
    
    int TOT = (int)total;
    
   // float total = result1 + result2;
    
    result.text = [NSString stringWithFormat:@"%i", TOT];

}

-(float) round:(float)num toSignificantFigures:(int)n {
    if(num == 0) {
        return 0;
    }
    
    double d = ceil(log10(num < 0 ? -num: num));
    int power = n - (int) d;
    
    double magnitude = pow(10, power);
    long shifted = round(num*magnitude);
    return shifted/magnitude;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)gotoBack:(id)sender
{
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"Ultimo-header-pagina-principal.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction) tapBackground: (id) sender{
	[anosCredito resignFirstResponder];

    [anticipo resignFirstResponder];

    [precioVenta resignFirstResponder];
    [interes resignFirstResponder];

	
	
	
}


@end
