//
//  Century21 HOME.h
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-01-28.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Mapas.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "Detalles.h"
//#import "Agentes.h"
//#import "MiPropiedad.h"
#import <MessageUI/MessageUI.h> 
#import "Favoritos.h"
#import "Reachability.h"

@class Mapas;
@class Detalles;
@class Agentes;
@class MiPropiedad, Favoritos,Reachability;;

@interface Century21_HOME : UIViewController<MFMailComposeViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UISearchBarDelegate>{
    
    Reachability* internetReachable;
    Reachability* hostReachable;
    
    bool hostActive;
    bool internetActive;
    
    UISearchBar *searchBar;
    UIView *socialesView;
    Mapas *mapas;
    Detalles *detView;
    Agentes *agent;
    MiPropiedad *miProp;
    Favoritos *favoritosC;
    UIButton *venta;
    UIButton *renta;
    UIButton *ambos;
    UIButton *calcu;
    UIButton *agente;
    UIButton *sociales;
    UIButton *contacto;
            UIButton *miPropiedad;
    UIButton *regresar;
    UIImageView *headerSociales;
    
    UIScrollView *scrollView;
    ///
    
    
    NSMutableArray *estados;
    NSMutableArray *ciudades;
    NSMutableArray *colonias;
    
    UIView *searchView;
    UIPickerView * estadosPicker;
    NSArray *estadosSearch;
    NSArray *ciudadesSearch;
    NSArray *coloniasSearch;
    
    UIImageView *notifCiudad;
    
    NSMutableArray * coloniaArr;
     NSMutableArray * ciudadArr;
     NSMutableArray * imageArr;
     NSMutableArray * agenteArr;
     NSMutableArray * precioArr;
    
    int searchControl;
    int actionSearchBool;
    int internetBool;
    int scrollBOOL;
    UIActivityIndicatorView *activityIndicator;
    UIActivityIndicatorView *activityIndicator2;
    UIButton *busqueda;
    
    UILabel *searchLabel;
    UILabel *searchLabel1;
    UILabel *searchLabel2;
    
    UILabel *searchSUB;
    UILabel *searchSUB1;
    UILabel *searchSUB2;

    
    UIImageView *searchHeader;
    UIImageView *searchHeader1;
    UIImageView *searchHeader2;
    
    UIButton *volPicker;
        UIButton *buscarPicker;
    
    NSTimer *timer;

}
 @property (nonatomic, retain)   NSTimer *timer;
@property (nonatomic, retain) IBOutlet UIButton *volPicker;
@property (nonatomic, retain) IBOutlet UIButton *buscarPicker;

@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;

@property (nonatomic, retain) IBOutlet UIButton *busqueda;
@property (nonatomic, retain) IBOutlet UIButton *venta;
@property (nonatomic, retain) IBOutlet UIButton *renta;
@property (nonatomic, retain) IBOutlet UIButton *ambos;
@property (nonatomic, retain) IBOutlet UIButton *calcu;
@property (nonatomic, retain) IBOutlet UIButton *agente;
@property (nonatomic, retain) IBOutlet UIButton *sociales;
@property (nonatomic, retain) IBOutlet UIButton *contacto;
@property (nonatomic, retain) IBOutlet UIButton *miPropiedad;

@property (nonatomic, retain) IBOutlet UIButton *regresar;
@property (nonatomic, retain) IBOutlet UIImageView *headerSociales;

@property (nonatomic, retain) IBOutlet UIView *socialesView;
@property (nonatomic, retain) UIScrollView *scrollView;

@property (nonatomic, retain)  Mapas *mapas;
 @property (nonatomic, retain)   Detalles *detView;
@property (nonatomic, retain)  Agentes *agent;
@property (nonatomic, retain) MiPropiedad *miProp;
@property (nonatomic, retain) Favoritos *favoritosC;

@property (nonatomic, retain)  NSMutableArray *estados;
@property (nonatomic, retain)  NSMutableArray *ciudades;
@property (nonatomic, retain)  NSMutableArray *colonias;

 @property (nonatomic, retain) IBOutlet   UIPickerView * estadosPicker;
@property (nonatomic, retain) NSArray *estadosSearch;
@property (nonatomic, retain) NSArray *ciudadesSearch;
@property (nonatomic, retain) NSArray *coloniasSearch;
@property (nonatomic, retain) IBOutlet  UIView *searchView;

@property (nonatomic, retain) IBOutlet    UIImageView *notifCiudad;
////Favs
@property  (strong,nonatomic) NSString *coloniaS;
@property  (strong,nonatomic) NSString *ciudadStr;
@property  (strong,nonatomic) NSString *imageS;
@property  (strong,nonatomic) NSString *AgenteS;
@property  (strong,nonatomic) NSString *precioS;
@property  (strong,nonatomic) NSString *FavORnot;

@property  (strong,nonatomic) NSString *estado;
@property  (strong,nonatomic) NSString *ciudad;
@property  (strong,nonatomic) NSString *colonia;
@property  (strong,nonatomic) NSString *procedence;
////
@property (nonatomic, retain) NSMutableArray * coloniaArr;
@property (nonatomic, retain) NSMutableArray * ciudadArr;
@property (nonatomic, retain) NSMutableArray * imageArr;
@property (nonatomic, retain) NSMutableArray * agenteArr;
@property (nonatomic, retain) NSMutableArray * precioArr;

@property (nonatomic, retain) IBOutlet  UILabel *searchLabel;
@property (nonatomic, retain) IBOutlet  UILabel *searchLabel1;
@property (nonatomic, retain) IBOutlet  UILabel *searchLabel2;
@property (nonatomic, retain) IBOutlet UIImageView *searchHeader;
@property (nonatomic, retain) IBOutlet UIImageView *searchHeader1;
@property (nonatomic, retain) IBOutlet UIImageView *searchHeader2;

@property (nonatomic, retain) IBOutlet   UILabel *searchSUB;
@property (nonatomic, retain) IBOutlet   UILabel *searchSUB1;
@property (nonatomic, retain) IBOutlet   UILabel *searchSUB2;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator2;


-(IBAction)pushAgent;
-(IBAction)pushCalc;
-(IBAction)pushMAPS;
-(IBAction)pushVenta;
-(IBAction)pushRenta;
-(IBAction)pushSociales;
- (IBAction) tapBackground: (id) sender;
-(IBAction)volver;
-(IBAction)pushMail;
-(IBAction)pushMiprop;

-(IBAction)pushSearch;
-(IBAction)pushPicker;
-(IBAction)volverPicker;
-(void)configScroll;
-(void)getFavoritos;

-(IBAction)timerAction;

-(void) checkNetworkStatus:(NSNotification *)notice;
@end
