//
//  Century21 HOME.m
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-01-28.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "Century21 HOME.h"
#import "GPSMapViewController.h"
#import "AgentesViewController.h"
#import "MiPropiedadViewController.h"
#import "DetailSocialViewController.h"
#import <MessageUI/MessageUI.h> 
#import "Calculadora.h"
#import "BusquedaAvanzada.h"
#import "Reachability.h"

@interface Century21_HOME ()

@end

@implementation Century21_HOME
@synthesize searchBar, activityIndicator2,timer, mapas, venta, renta, ambos, agente, calcu, sociales, socialesView, detView, scrollView, agent, contacto, miPropiedad, miProp, regresar, headerSociales, estados, ciudades, colonias, estadosSearch, ciudadesSearch, coloniasSearch, estadosPicker, searchView, notifCiudad, favoritosC, coloniaArr, ciudadArr, agenteArr, precioArr, imageArr, activityIndicator, busqueda, searchLabel, searchHeader, searchHeader1, searchHeader2, searchLabel1, searchLabel2, searchSUB, searchSUB1, searchSUB2, volPicker, buscarPicker, procedence;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      
        ////////
       
    }
    return self;
}


- (NSArray *)reverseMutArray:(NSMutableArray *)inputArray {
    if ([inputArray count] == 0)
        return inputArray;
    NSUInteger i = 0;
    NSUInteger j = [inputArray count] - 1;
    while (i < j) {
        [inputArray exchangeObjectAtIndex:i
                        withObjectAtIndex:j];
        
        i++;
        j--;
    }
    return inputArray;
}

- (NSArray *)reversedArray:(NSArray *)inputArray  {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[inputArray count]];
    NSEnumerator *enumerator = [inputArray reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    return array;
}



- (void)downloadImage:(NSString *)inputURL inBackground:(UIImageView *)imageView
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(imageView.bounds.size.width/2, imageView.bounds.size.height/2)];
    [imageView addSubview:activity];
    [activity startAnimating];
    
    //  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://i1065.photobucket.com/albums/u395/lkronecker/%@",image]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",inputURL]];
    
    
    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
    dispatch_async(download_queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *_image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            [activity removeFromSuperview];
            [imageView setImage:_image];
        });
    });
    // dispatch_release(download_queue);
}


-(void)getFavoritos{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.coloniaArr = [[NSMutableArray alloc]initWithArray:[userDefaults objectForKey:@"colData"]];
    self.ciudadArr = [[NSMutableArray alloc]initWithArray:[userDefaults objectForKey:@"ciuData"]];
    self.imageArr = [[NSMutableArray alloc]initWithArray:[userDefaults objectForKey:@"imgData"]];
    self.agenteArr = [[NSMutableArray alloc]initWithArray:[userDefaults objectForKey:@"agenData"]];
    self.precioArr = [[NSMutableArray alloc]initWithArray:[userDefaults objectForKey:@"precData"]];
    
    
    [self.coloniaArr addObject:self.coloniaS];
    [userDefaults setObject:self.coloniaArr forKey:@"colData"];
    self.coloniaArr = [userDefaults objectForKey:@"colData"];
    ///
    [self.ciudadArr addObject:self.ciudadStr];
    [userDefaults setObject:self.ciudadArr forKey:@"ciuData"];
    self.ciudadArr = [userDefaults objectForKey:@"ciuData"];
    ////
 //   if ([self.procedence isEqualToString:@"1"]) {
  //  }else{
    [self.imageArr addObject:self.imageS];
    [userDefaults setObject:self.imageArr forKey:@"imgData"];
        self.imageArr = [userDefaults objectForKey:@"imgData"];
   // }
    ///
    [self.agenteArr addObject:self.AgenteS];
    [userDefaults setObject:self.agenteArr forKey:@"agenData"];
    self.agenteArr = [userDefaults objectForKey:@"agenData"];
    ///
    
    [self.precioArr addObject:self.precioS];
    [userDefaults setObject:self.precioArr forKey:@"precData"];
    self.precioArr = [userDefaults objectForKey:@"precData"];
    
    

    
    // synchronize is only needed while debugging (when you use the stop button in Xcode)
    // you don't need this in production code. remove it for release
    [userDefaults synchronize];
    
    NSLog(@"array:: %@, %@, %@, %@, %@", [userDefaults objectForKey:@"colData"],[userDefaults objectForKey:@"cuiData"], [userDefaults objectForKey:@"imgData"], [userDefaults objectForKey:@"agenData"], [userDefaults objectForKey:@"precData"]);


     
    
}
-(void)configScroll{
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            CGRect scrollViewFrame = CGRectMake(0, 335, 320, 67);
            self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            CGRect scrollViewFrame = CGRectMake(0, 410, 320, 67);
            self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];        }
    }
    
    
    
    [self.view addSubview:self.scrollView];
    CGSize scrollViewContentSize = CGSizeMake(320, 400);
    [self.scrollView setContentSize:scrollViewContentSize];
    //   scrollView.delegate = self;
    
    [self.scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.backgroundColor = nil;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUInteger nimages = 0;
    CGFloat cx = 0;
    int index = [[userDefaults objectForKey:@"colData"] count];
    for (int i = 0 ; i<index ; i++) {
        
        
        
        ////Base View
        UIView *base = [[UIView alloc]init];
        CGRect rectBase = base.frame;
        rectBase.size.height = 67
        ;
        rectBase.size.width = 251;
        rectBase.origin.x = ((scrollView.frame.size.width - (base.frame.size.width))/2)*nimages + cx+77;
        rectBase.origin.y = 0;
        
        base.frame = rectBase;
        // base.backgroundColor = [UIColor blackColor];
        cx += scrollView.frame.size.width;
        
        [scrollView addSubview:base];
        
        ////ImageView
        if (internetBool==0) {
            
            UIImage *image = [UIImage imageNamed:@"Imagen-no-disponible.png"];
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            
            CGRect rect = imageView.frame;
            rect.size.height = 67
            ;
            rect.size.width = 100;
            rect.origin.x = 0;
            rect.origin.y = 0;
            
            imageView.frame = rect;
            
            [base addSubview:imageView];
            
        }else{
        NSArray *images = [self reversedArray:[userDefaults objectForKey:@"imgData"]];
        NSString *imageName = [NSString stringWithFormat:@"%@",[images objectAtIndex:i]];
        UIImage *image = [UIImage imageNamed:@"Imagen-cargando.png"];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        
        [self downloadImage:imageName inBackground:imageView];
        
        CGRect rect = imageView.frame;
        rect.size.height = 67
        ;
        rect.size.width = 100;
        rect.origin.x = 0;
        rect.origin.y = 0;
        
        imageView.frame = rect;
        
        [base addSubview:imageView];
        }
        
        ///Labels
        //agente
        UILabel *labelAG = [[UILabel alloc]initWithFrame:CGRectMake(105, 0, 151, 17)];
        NSArray *agentes = [self reversedArray:[userDefaults objectForKey:@"agenData"]];
        labelAG.text = [NSString stringWithFormat:@"%@", [agentes objectAtIndex:i]];
        [base addSubview:labelAG];
        labelAG.backgroundColor = [UIColor clearColor];
        labelAG.textColor = [UIColor whiteColor];
        [labelAG setFont:[UIFont fontWithName:@"Avenir-Black" size:16]];
        
        UILabel *labelCIU = [[UILabel alloc]initWithFrame:CGRectMake(105, 17, 151, 17)];
        NSArray *ciudades1 = [self reversedArray:[userDefaults objectForKey:@"cuiData"]];
     //   labelCIU.text = [NSString stringWithFormat:@"%@", [ciudades1 objectAtIndex:i]];
        [base addSubview:labelCIU];
        labelCIU.backgroundColor = [UIColor clearColor];
        labelCIU.textColor = [UIColor whiteColor];
        
        UILabel *labelCOL = [[UILabel alloc]initWithFrame:CGRectMake(105, 34, 151, 17)];
        NSArray *colonias1 = [self reversedArray:[userDefaults objectForKey:@"colData"]];
        labelCOL.text = [NSString stringWithFormat:@"%@", [colonias1 objectAtIndex:i]];
        [base addSubview:labelCOL];
        labelCOL.backgroundColor = [UIColor clearColor];
        [labelCOL setFont:[UIFont fontWithName:@"Avenir-Heavy" size:13]];
        labelCOL.textColor = [UIColor whiteColor];
        
        UILabel *labelPRE = [[UILabel alloc]initWithFrame:CGRectMake(105, 51, 151, 17)];
        NSArray *precios = [self reversedArray:[userDefaults objectForKey:@"precData"]];
        labelPRE.text = [NSString stringWithFormat:@"%@", [precios objectAtIndex:i]];
        [base addSubview:labelPRE];
        labelPRE.backgroundColor = [UIColor clearColor];
        [labelPRE setFont:[UIFont fontWithName:@"Avenir-Heavy" size:13]];
        labelPRE.textColor = [UIColor whiteColor];
        
        
        
        
    }
    
    
    
    [scrollView setContentSize:CGSizeMake(cx, [scrollView bounds].size.height)];
    
    
    
    
}

- (void)viewDidUnload{
   
    
}

-(void)viewDidAppear:(BOOL)animated{
 
   // self.activityIndicator.hidden=NO;
    
}

-(void)getFavoritos2{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *anArr = [userDefaults objectForKey:@"somedata"];
    NSLog(@"anArr::: %@",anArr);

}
-(void)viewWillAppear:(BOOL)animated{
    
    
    
 // [self.activityIndicator startAnimating];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
    
    // check if a pathway to a random host exists 
    hostReachable = [Reachability reachabilityWithHostName:@"http://century21.elolabs.com"];
    [hostReachable startNotifier];
    
  /////
        
//////
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    scrollBOOL = 0;
    socialesView.alpha = 0;
    headerSociales.alpha = 0;
    searchBar.alpha = 1;
    notifCiudad.alpha = 0;
  //  self.scrollView = nil;
    
    searchHeader.alpha = 0;
    searchLabel.alpha =0;
    
    searchHeader1.alpha = 0;
    searchLabel1.alpha =0;
    
    searchHeader2.alpha = 0;
    searchLabel2.alpha =0;
    
    searchSUB.alpha =0;
    searchSUB1.alpha =0;
    searchSUB2.alpha =0;

    
    self.searchLabel.text = @"ESCOGE TU ESTADO";
        self.searchLabel1.text = @"ESCOGE TU DELEGACION O MUNICIPIO";
        self.searchLabel2.text = @"ESCOGE TU COLONIA";
    
    searchLabel.textColor = [UIColor whiteColor];
    searchLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];

    searchLabel1.textColor = [UIColor whiteColor];
    searchLabel1.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];
    searchLabel2.textColor = [UIColor whiteColor];
    searchLabel2.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];


}

- (void)viewDidLoad
{
 //   [self dateConfig];
 //   [self.view bringSubviewToFront:self.activityIndicator];
   // [self.activityIndicator startAnimating];
   
   self.activityIndicator2.hidden=YES;

    scrollBOOL = 1;
    
     //  self.activityIndicator.hidden = YES;
    searchControl = 0;
    actionSearchBool = 0;
    searchHeader.alpha = 0;
    searchLabel.alpha =0;
    
    searchHeader1.alpha = 0;
    searchLabel1.alpha =0;
    
    searchHeader2.alpha = 0;
    searchLabel2.alpha =0;
    
    searchSUB.alpha =0;
    searchSUB1.alpha =0;
    searchSUB2.alpha =0;
    
  //  self.searchLabel.text = self.estado;

    

     //  [self JSONrequest];
   
    
    socialesView.alpha = 0;
    headerSociales.alpha = 0;
        searchView.alpha = 0;
    notifCiudad.alpha = 0;
 /////////
    
    UIGraphicsBeginImageContext (self.searchBar.frame.size);
    [[UIImage imageNamed:@"Header-agente-inmobiliario.png"] drawInRect:self.searchBar.bounds];
    UIImage *imageSearchBar = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [[UISearchBar appearance] setSearchFieldBackgroundImage:imageSearchBar forState:UIControlStateNormal];
    [[UISearchBar appearance] setBackgroundImage:imageSearchBar];
    
    ////
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(volver) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    self.regresar = button;
    //  [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    regresar.hidden = YES;
    
    
      ///////////////////////
        UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"Ultimo-header-pagina-principal.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    
    /////////
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"Fondo-pagina-principal.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
  //  self.socialesView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self.view bringSubviewToFront:self.searchView];
    

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)pushMAPS{
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{

  //  self.scrollView.hidden = YES;
  //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    GPSMapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"Mapas"];
    map.MapString = @"2";
    [self.navigationController pushViewController:map animated:YES];
  
//map.MapString = @"2";
     //  [mp release];
    
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];

    }

}

-(IBAction)pushAgent{
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{

    AgentesViewController *ag = [self.storyboard instantiateViewControllerWithIdentifier:@"AG"];
  //  map.MapString = @"2";
    [self.navigationController pushViewController:ag animated:YES];
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];

}
}

-(IBAction)pushCalc{
    
    Calculadora *map = [self.storyboard instantiateViewControllerWithIdentifier:@"calcu"];
  //  map.MapString = @"0";
    [self.navigationController pushViewController:map animated:YES];
///////
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];

}
-(IBAction)pushVenta{
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{

  //  self.scrollView.hidden = YES;
   // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    GPSMapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"Mapas"];
    map.MapString = @"0";
    [self.navigationController pushViewController:map animated:YES];
    ////
    Detalles *det = [[Detalles alloc]init];
    det.ventRentST = @"0";
    self.detView =det;
  //  [det release];

    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];

}
}
-(IBAction)pushRenta{
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{

  //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    GPSMapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"Mapas"];
    map.MapString = @"1";
    [self.navigationController pushViewController:map animated:YES];
    
    ////
    
    Detalles *det = [[Detalles alloc]init];
    det.ventRentST = @"1";
    self.detView =det;
   // [det release];

    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    }
}
-(IBAction)pushMail{
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{

    NSString *url = [NSString stringWithFormat: @"mailto:pruebaAPP@century21.com?&subject=&body="];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}
}

-(IBAction)pushMiprop{
  
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{

    MiPropiedadViewController *miPropied = [self.storyboard instantiateViewControllerWithIdentifier:@"MP"];
   // map.MapString = @"1";
    [self.navigationController pushViewController:miPropied animated:YES];
    /////
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
}
}

-(IBAction)pushSociales{
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{

    regresar.hidden = NO;
    headerSociales.alpha = 1;
    searchBar.alpha = 0;
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    headerSociales.alpha = 1;
    searchBar.alpha = 0;
    
	venta.alpha = 1.0;
	CGRect ventaFrame = venta.frame;
	ventaFrame.origin.y -= 150;
    ventaFrame.origin.x -= 150;
	venta.frame = ventaFrame;
	//[UIView commitAnimations];
  //  [UIView beginAnimations:@"advancedAnimations" context:nil];
	//[UIView setAnimationDuration:0.6];
    
	ambos.alpha = 1.0;
	CGRect ambosFrame = ambos.frame;
	ambosFrame.origin.y -= 150;
    ambosFrame.origin.x += 150;
	ambos.frame = ambosFrame;

    calcu.alpha = 1.0;
	CGRect calcFrame = calcu.frame;
	calcFrame.origin.y -= 150;
    calcFrame.origin.x += 150;
	calcu.frame = calcFrame;

    
    sociales.alpha = 1.0;
	CGRect socialesFrame = sociales.frame;
	socialesFrame.origin.y += 150;
    socialesFrame.origin.x += 150;
	sociales.frame = socialesFrame;
    
    renta.alpha = 1.0;
	CGRect rentaFrame = renta.frame;
	rentaFrame.origin.y -= 150;
	renta.frame = rentaFrame;

    agente.alpha = 1.0;
	CGRect agenteFrame = agente.frame;
	agenteFrame.origin.y += 300;
	agente.frame = agenteFrame;
    
    contacto.alpha = 1.0;
	CGRect contFrame = contacto.frame;
	contFrame.origin.y += 300;
	contacto.frame = contFrame;
    
    miPropiedad.alpha = 1.0;
	CGRect proFrame = miPropiedad.frame;
	proFrame.origin.y += 300;
	miPropiedad.frame = proFrame;

    
     self.socialesView.alpha =1;
    
	[UIView commitAnimations];
    
    
    
    }

}

- (IBAction) tapBackground: (id) sender{
	[searchBar resignFirstResponder];
	
	
	
}

-(IBAction)volver{
    regresar.hidden = YES;
    

    self.searchLabel.text = @"";
    self.searchLabel1.text = @"";
    self.searchLabel2.text = @"";
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    headerSociales.alpha = 0;
    searchBar.alpha = 1;
    
	venta.alpha = 1.0;
	CGRect ventaFrame = venta.frame;
	ventaFrame.origin.y += 150;
    ventaFrame.origin.x += 150;
	venta.frame = ventaFrame;
	//[UIView commitAnimations];
    //  [UIView beginAnimations:@"advancedAnimations" context:nil];
	//[UIView setAnimationDuration:0.6];
    
	ambos.alpha = 1.0;
	CGRect ambosFrame = ambos.frame;
	ambosFrame.origin.y += 150;
    ambosFrame.origin.x -= 150;
	ambos.frame = ambosFrame;
    
    calcu.alpha = 1.0;
	CGRect calcFrame = calcu.frame;
	calcFrame.origin.y += 150;
    calcFrame.origin.x -= 150;
	calcu.frame = calcFrame;
    
    
    sociales.alpha = 1.0;
	CGRect socialesFrame = sociales.frame;
	socialesFrame.origin.y -= 150;
    socialesFrame.origin.x -= 150;
	sociales.frame = socialesFrame;
    
    renta.alpha = 1.0;
	CGRect rentaFrame = renta.frame;
	rentaFrame.origin.y += 150;
	renta.frame = rentaFrame;
    
    agente.alpha = 1.0;
	CGRect agenteFrame = agente.frame;
	agenteFrame.origin.y -= 300;
	agente.frame = agenteFrame;
    
    contacto.alpha = 1.0;
	CGRect contFrame = contacto.frame;
	contFrame.origin.y -= 300;
	contacto.frame = contFrame;
    
    miPropiedad.alpha = 1.0;
	CGRect proFrame = miPropiedad.frame;
	proFrame.origin.y -= 300;
	miPropiedad.frame = proFrame;

    
    socialesView.alpha =0;
    
	[UIView commitAnimations];
    

}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  //  slideControl = 0;
    NSLog(@"heeey");
    if ([segue.identifier isEqualToString:@"goToFB"]) {
        DetailSocialViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.facebook.com/Century21Mexico";
        
        self.socialesView.alpha = 0;
        self.regresar.hidden = YES;
        headerSociales.alpha = 0;
        searchBar.alpha = 1;
        
        
    }
    else if ([segue.identifier isEqualToString:@"goToTwittwer"]) {
        DetailSocialViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.twitter.com/c21mexico";
        
                self.socialesView.alpha = 0;
                self.regresar.hidden = YES;
        headerSociales.alpha = 0;
        searchBar.alpha = 1;
        
    }
    else if ([segue.identifier isEqualToString:@"Busqueda"]) {
        BusquedaAvanzada *destViewController = segue.destinationViewController;
        NSInteger row;
        row = [self.estadosPicker selectedRowInComponent:0];
        
        destViewController.estado= [self.estadosSearch objectAtIndex:row];
        
        self.socialesView.alpha = 0;
        self.regresar.hidden = YES;
        headerSociales.alpha = 0;
        searchBar.alpha = 1;
        
    }
}

-(IBAction)pushSearch{
    
   // [self.activityIndicator startAnimating];
    
    self.activityIndicator2.hidden=NO;
    
    if (actionSearchBool == 0) {
    
    self.activityIndicator.hidden=NO;
        searchControl = 1;
        NSInteger row;
        row = [self.estadosPicker selectedRowInComponent:0];
        
        NSString *ciud= [NSString stringWithFormat:@"%@",[self.estadosSearch objectAtIndex:row] ];
        self.estado = ciud;
        
        [self JSONrequest];
        
        NSLog(@"%@, ", self.estado);
        actionSearchBool = 1;
        
        UIImage *btnImage = [UIImage imageNamed:@"FiBotón-anterior.png"];
        [self.volPicker setImage:btnImage forState:UIControlStateNormal];
        
      //  UIImage *colonia = [UIImage imageNamed:@"Escoge-tu-estado.png"];
        
        
      //  [self.notifCiudad setImage:colonia];
        
        self.searchLabel.text =[NSString stringWithFormat:@"%@", self.estado];
        searchLabel.textColor = [UIColor yellowColor];
        searchLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16.0)];
        
        searchHeader1.alpha = 1;
        searchLabel1.alpha =1;
        searchSUB.alpha =1;
        
        
    }
    else if (actionSearchBool == 1) {
        
        
        searchControl = 2;
        NSInteger row;
        row = [self.estadosPicker selectedRowInComponent:0];
        
        NSString *ciud= [NSString stringWithFormat:@"%@",[self.ciudadesSearch objectAtIndex:row] ];
        self.ciudad = ciud;
        
        [self JSONrequest];
        
        NSLog(@"%@, ", self.ciudad);
        actionSearchBool = 2;
        
      //  UIImage *colonia = [UIImage imageNamed:@"Escoge-tu-ciudad.png"];
        
      //  [self.notifCiudad setImage:colonia];
        
        self.searchLabel1.text =[NSString stringWithFormat:@"%@", self.ciudad];
        
        searchLabel1.textColor = [UIColor yellowColor];
        searchLabel1.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16.0)];
        
        searchHeader2.alpha = 1;
        searchLabel2.alpha =1;
        searchSUB1.alpha =1;
    }else{
        searchControl = 0;
         actionSearchBool = 0;
        
         self.busqueda.enabled = YES;
        
        NSInteger row;
        row = [self.estadosPicker selectedRowInComponent:0];
        
        NSString *col= [NSString stringWithFormat:@"%@",[self.coloniasSearch objectAtIndex:row] ];
        self.colonia = col;
        
       // UIImage *colonia = [UIImage imageNamed:@"Escoge-tu-colonia.png"];
        
        //[self.notifCiudad setImage:colonia];
        
        self.searchLabel2.text =[NSString stringWithFormat:@"%@", self.colonia];
        searchSUB2.alpha =1;

        searchLabel2.textColor = [UIColor yellowColor];
        searchLabel2.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16.0)];

        
        NSLog(@"%@; %@; %@", self.estado, self.ciudad, self.colonia);
        
        NSString *searchResSTR = [NSString stringWithFormat:@"http://century21.elolabs.com:8888/search?q=&estado=%@&ciudad=%@&colonia=%@", self.estado, self.ciudad, self.colonia];
        
        searchResSTR = [searchResSTR stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        
        NSLog(@"%@", searchResSTR);
        
        GPSMapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"Mapas"];
        map.MapString = @"3";
        map.searchURL = searchResSTR;
        map.estadoST =self.estado;
        map.ciudadST = self.ciudad;
        map.coloniaST = self.colonia;
        
        [self.navigationController pushViewController:map animated:YES];
        
        
        UIGraphicsBeginImageContext (CGSizeMake(320, 44));
        [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
        UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
        searchHeader.alpha = 0;
        searchLabel.alpha =0;
        //searchSUB2.alpha = 1;
        
         [self JSONrequest];

        
    }
    
    self.activityIndicator.hidden=YES;
    
}

-(IBAction)pushPicker{
    
  //  self.activityIndicator.hidden=NO;
    
    if (internetBool == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                        message:@"Verifique su conexión a Internet."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }else{
    
        scrollView.hidden = YES;
    searchHeader.alpha = 1;
    searchLabel.alpha =1;
   // searchSUB.alpha = 1;
    
    
  //
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    searchView.alpha = 1.0;
	CGRect proFrame = searchView.frame;
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
           proFrame.origin.y -= 193;
        }else if (iOSDeviceScreenSize.height == 568){
           proFrame.origin.y -= 260;
        }
    }

	
	searchView.frame = proFrame;
	
	
    
	[UIView commitAnimations];
    
    [self pushSociales];
    
      self.socialesView.alpha =0;
    notifCiudad.alpha = 1;
    
    self.busqueda.enabled = NO;
    
    self.regresar.hidden = YES;


    }
    
  //  
}

-(IBAction)volverPicker{
    
    if(actionSearchBool==0){
    
        
        scrollView.hidden= NO;
    searchHeader.alpha = 0;
    searchLabel.alpha =0;
    searchHeader1.alpha = 0;
    searchLabel1.alpha =0;
    
    searchHeader2.alpha = 0;
    searchLabel2.alpha =0;
    
    // searchLabel.text = @"";
        
        self.searchLabel.text = @"ESCOGE TU ESTADO";
        searchLabel.textColor = [UIColor whiteColor];
        searchLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];
        
         self.searchLabel1.text = @"ESCOGE TU DELEGACION O MUNICIPIO";
        searchLabel1.textColor = [UIColor whiteColor];
        searchLabel1.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];
        
        self.searchLabel2.text = @"ESCOGE TU COLONIA";
        searchLabel2.textColor = [UIColor whiteColor];
        searchLabel2.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];

    //searchLabel1.text = @"";
    //searchLabel2.text = @"";
    
    searchSUB.alpha =0;
    searchSUB1.alpha =0;
    searchSUB2.alpha =0;
    
      //  self.regresar.hidden = NO;
    
        self.busqueda.enabled = YES;
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    searchView.alpha = 0;
	CGRect proFrame = searchView.frame;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                proFrame.origin.y += 193;
            }else if (iOSDeviceScreenSize.height == 568){
                proFrame.origin.y += 260;
            }
        }

	searchView.frame = proFrame;
    
	[UIView commitAnimations];
    
    [self volver];

    notifCiudad.alpha = 0;
    actionSearchBool = 0;
    searchControl = 0;
    
    [self.estadosPicker reloadAllComponents];
    [self.estadosPicker reloadInputViews];
        
        searchHeader.alpha = 0;
       // searchLabel.alpha =0;
       // searchSUB.alpha =0;
    
}
    else if(actionSearchBool == 1){
        
        UIImage *btnImage = [UIImage imageNamed:@"Cancelar-black.png"];
        [self.volPicker setImage:btnImage forState:UIControlStateNormal];
        
        actionSearchBool = 0;
        searchControl = 0;
        
        [self.estadosPicker reloadAllComponents];
        [self.estadosPicker reloadInputViews];
        
        searchHeader1.alpha = 0;
        searchLabel1.alpha =0;
        self.searchLabel.text = @"ESCOGE TU ESTADO";
        searchLabel.textColor = [UIColor whiteColor];
        searchLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];

     searchSUB.alpha =0;
    
    }
    else if(actionSearchBool == 2){
        
        actionSearchBool = 1;
        searchControl = 1;
        
        [self.estadosPicker reloadAllComponents];
        [self.estadosPicker reloadInputViews];
        
      //  searchLabel1.textColor = [UIColor yellowColor];
       // searchLabel1.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16.0)];
        
        searchSUB1.alpha = 0;
        
        searchHeader2.alpha = 0;
        searchLabel2.alpha =0;
        self.searchLabel1.text = @"ESCOGE TU DELEGACION O MUNICIPIO";
        searchLabel1.textColor = [UIColor whiteColor];
        searchLabel1.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];
        
        self.searchLabel2.text = @"ESCOGE TU COLONIA";
        searchLabel2.textColor = [UIColor whiteColor];
        searchLabel2.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(11.0)];

     //  

        
        
    }

}

-(IBAction)postMail{
    
 //   NSString *urlST = [NSString stringWithFormat:@"%@", [self.urlArray objectAtIndex:[self.receta intValue]-1]];
  //  NSString *subjST = [NSString stringWithFormat:@"%@", [self.titulosRecetas objectAtIndex:[self.receta intValue]-1]];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    NSArray *recipients = [[NSArray alloc]initWithObjects:@"informes@century21mexico.com",@"alfredo@century21mexico.com",@"patricia@century21mexico.com",@"contacto@century21mexico.com", nil];
    
    [picker setToRecipients:recipients];
    [picker setSubject:[NSString stringWithFormat:@"Contacto"]];
    [picker setMessageBody:[NSString stringWithFormat:@"contacto Century 21"] isHTML:YES];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)JSONrequest {
    
  //  self.activityIndicator.hidden=NO;
    //[self.activityIndicator startAnimating];
        
        NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/update_colonias"]];

        NSData* data = [NSData dataWithContentsOfURL:
                        url];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
    
   
    }

- (void)fetchedData:(NSData *)responseData {
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSDictionary* EstadosDict = [json objectForKey:@"estados"]; //2
    NSArray* estadosA = [[json objectForKey:@"estados"] allKeys];
    
    self.estadosSearch = estadosA;
    
    NSDictionary* CiudadesDict = [[EstadosDict objectForKey:self.estado] objectForKey:@"ciudades"] ;
    NSArray* ciudadesA =  [[[EstadosDict objectForKey:self.estado] objectForKey:@"ciudades"] allKeys] ;
    
    self.ciudadesSearch = ciudadesA;
    
    // NSDictionary* ColoniasDict = [[CiudadesDict objectForKey:self.ciudad] objectForKey:@"colonias"] ;
    NSArray* coloniasA = [[[CiudadesDict objectForKey:self.ciudad] objectForKey:@"colonias"] allObjects];
    // NSArray *array = [NSArray array];
    self.coloniasSearch = coloniasA;
    
  
    
    
   // NSLog(@"ESTADOS:%@, CIUDADES:%@, COLONIAS:%@",estadosA, ciudadesA, coloniasA); //3
    
    [self.estadosPicker reloadAllComponents];
    [self.estadosPicker reloadInputViews];
    
   
   //   [self.activityIndicator stopAnimating];
   
    self.activityIndicator.hidden=YES;
  //  
    timer = [NSTimer scheduledTimerWithTimeInterval:0.67 target:self selector:@selector(timerAction) userInfo:nil repeats:NO];
       }
       
       -(IBAction)timerAction{
       self.activityIndicator2.hidden=YES;
       }


#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	 if (searchControl == 0) {
        return [self.estadosSearch count];
     }
   else if (searchControl == 1) {
        return [self.ciudadesSearch count];
    }else{
        return [self.coloniasSearch count];
    }
}
#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (searchControl == 0) {
           return [self.estadosSearch objectAtIndex:row];
    }
   else if (searchControl == 1) {
        return [self.ciudadesSearch objectAtIndex:row];
    }else{
        return [self.coloniasSearch objectAtIndex:row];
    }
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    NSLog(@"");
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    return YES;
}

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            internetActive = NO;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Imposible descargar datos."
                                                            message:@"Verifique su conexión a Internet."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            internetBool = 0;
                        
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            internetActive = YES;
            internetBool = 1;
            [self JSONrequest];
            
            
            if (scrollBOOL == 1) {
                
                if ([self.FavORnot isEqualToString:@"1"]) {
                    [self getFavoritos];
                    [self configScroll];
                }else{
                    
                    [self configScroll];
                }
            }

            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            internetActive = YES;
            internetBool = 1;
            [self JSONrequest];
            
            if (scrollBOOL == 1) {
                
                if ([self.FavORnot isEqualToString:@"1"]) {
                    [self getFavoritos];
                    [self configScroll];
                }else{
                    
                    [self configScroll];
                }
            }

                        
            break;
        }
    }
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    {
        case NotReachable:
        {
            NSLog(@"A gateway to the host server is down.");
            hostActive = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"A gateway to the host server is working via WIFI.");
            hostActive = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"A gateway to the host server is working via WWAN.");
            hostActive = YES;
            
            break;
        }
    }
}

@end
