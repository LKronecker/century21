//
//  Detalles.h
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-01-30.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h> 
#import "DetallesAgente.h"
#import "Favoritos.h"
#import "Century21 HOME.h"

@class DetallesAgente, Favoritos, Century21_HOME;

@interface Detalles : UIViewController<MFMailComposeViewControllerDelegate, UIScrollViewDelegate, UIAlertViewDelegate>{
    int agenteIntGen;
    IBOutlet UILabel *adressLabel;
    IBOutlet UILabel *subtitleLabel;
    IBOutlet UILabel *fulAdressLabel;
    IBOutlet UILabel *priceLabel;
    
    IBOutlet UILabel *calleLabel;
    IBOutlet UILabel *colLabel;
    IBOutlet UILabel *descLabel;
    IBOutlet UILabel *tipoLabel;
    IBOutlet UILabel *ciudadLabel;
    IBOutlet UILabel *estadoLabel;
    IBOutlet UILabel *cpLabel;
    IBOutlet UILabel *RenVenLabel;
    
     UIImageView *imageCasa;
    
    NSMutableArray *imagesVenta;
    NSMutableArray *fulAdress;
    NSMutableArray *precio;
    NSMutableArray *calle;
    NSMutableArray *colonia;
    NSMutableArray *descripcion;
    NSMutableArray *tipo;
    NSMutableArray *ciudad;
    NSMutableArray *estado;
    NSMutableArray *CP;
    NSMutableArray *images;
    NSMutableArray *proposito;
    NSMutableArray *adress;
    NSMutableArray *agentes;
    NSMutableArray *agentesInTS;
    NSMutableArray *agentesID;
    
    
    UIView *socialesView;
    
    UIScrollView *scrollView;
    
    
    NSString *nombreAgente;
    DetallesAgente *detA;
    Favoritos *favoritosC;
    Century21_HOME *home;
    
    
    UIImageView *imagNoDisp;

    UIActivityIndicatorView *activityIndicator;
}
@property (nonatomic, retain)    DetallesAgente *detA;
@property (nonatomic, retain) Favoritos *favoritosC;
@property (nonatomic,retain) NSString *labelText;
@property (nonatomic, retain) NSString *subtittleString;
@property (nonatomic, retain) NSString *latitudeST;
@property (nonatomic, retain) NSString *longitudeST;
@property (nonatomic, retain) NSString *imageST;
@property (nonatomic, retain) NSString *ventRentST;

@property (nonatomic, retain) NSString *nombreAgente;

@property (nonatomic, retain) NSString *agenteST;

@property (nonatomic,retain) NSString *agenteINT;

@property (nonatomic, retain) UIScrollView *scrollView;

@property (nonatomic, retain)  NSMutableArray *images;
@property (nonatomic, retain) NSMutableArray *imagesVenta;
@property (nonatomic, retain) NSMutableArray *fulAdress;
@property (nonatomic, retain) NSMutableArray *precio;
@property (nonatomic, retain)  NSMutableArray *calle;
@property (nonatomic, retain)  NSMutableArray *colonia;
@property (nonatomic, retain)  NSMutableArray *descripcion;
@property (nonatomic, retain)  NSMutableArray *tipo;
@property (nonatomic, retain)  NSMutableArray *ciudad;
@property (nonatomic, retain)  NSMutableArray *estado;
@property (nonatomic, retain)  NSMutableArray *CP;
@property (nonatomic, retain)  NSMutableArray *proposito;
@property (nonatomic, retain) NSMutableArray *agentes;
@property (nonatomic, retain) NSMutableArray *adress;
@property (nonatomic, retain) NSMutableArray *agentesInTS;
@property (nonatomic, retain) NSMutableArray *agentesID;

  @property (nonatomic, retain) IBOutlet UIImageView *imageCasa;
  @property (nonatomic, retain) IBOutlet  UIView *socialesView;
  @property (nonatomic, retain) IBOutlet  UIImageView *imagNoDisp;
     //  @property (nonatomic, retain) NSArray *precio;

@property  (strong,nonatomic) NSString *searchURL;

@property  (strong,nonatomic) NSString *estadoS;
@property  (strong,nonatomic) NSString *ciudadS;
@property  (strong,nonatomic) NSString *coloniaS;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;


-(IBAction)gotoBack:(id)sender;
-(IBAction)goToMaps;
-(IBAction)shareNow;
-(IBAction)shareClose;
-(IBAction)favoritos;
-(IBAction)pushAgente;
@end
