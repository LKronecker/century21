//
//  Detalles.m
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-01-30.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "Detalles.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "DetallesAgente.h"
#import "Favoritos.h"

@interface Detalles ()

@end

@implementation Detalles
@synthesize labelText, agentesInTS, subtittleString, latitudeST, longitudeST, fulAdress, imagesVenta, imageST, imageCasa, ventRentST, agenteINT, precio, socialesView, calle, colonia, descripcion, tipo, ciudad, CP, estado, images, scrollView, agentes, agenteST, nombreAgente, detA, adress, proposito, imagNoDisp, activityIndicator, favoritosC, agentesID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (NSArray *)reverseMutArray:(NSMutableArray *)inputArray {
    if ([inputArray count] == 0)
        return inputArray;
    NSUInteger i = 0;
    NSUInteger j = [inputArray count] - 1;
    while (i < j) {
        [inputArray exchangeObjectAtIndex:i
                        withObjectAtIndex:j];
        
        i++;
        j--;
    }
    return inputArray;
}

- (NSArray *)reversedArray:(NSArray *)inputArray  {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[inputArray count]];
    NSEnumerator *enumerator = [inputArray reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    return array;
}



- (void)downloadImage:(NSString *)inputURL inBackground:(UIImageView *)imageView
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(imageView.bounds.size.width/2, imageView.bounds.size.height/2)];
    [imageView addSubview:activity];
    [activity startAnimating];
    
    //  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://i1065.photobucket.com/albums/u395/lkronecker/%@",image]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",inputURL]];
    
    
    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
    dispatch_async(download_queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *_image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            [activity removeFromSuperview];
            [imageView setImage:_image];
        });
    });
    // dispatch_release(download_queue);
}

-(void)viewDidAppear:(BOOL)animated{
    
    NSLog(@"try:%@", self.subtittleString);
    
 //   UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
  //  navCon.navigationItem.title = self.subtittleString;
    
   // self.navigationController.navigationBar.topItem.title = self.subtittleString;
    
    if([ventRentST isEqualToString:@"0"]){
        
        // NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@&tipo=venta", la, lo]];
        
        NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@&tipo=venta", latitudeST, longitudeST]];
        
        //    NSLog(@"%@, %@, %@", url, la, lo);
        
        NSData* data = [NSData dataWithContentsOfURL:
                        url];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
        RenVenLabel.text = @"VENTA";
        
        
    }
    if([ventRentST isEqualToString:@"1"]){
        
        // NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@&tipo=renta", la, lo]];
        
        NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@&tipo=renta", latitudeST, longitudeST]];
        
        //     NSLog(@"%@, %@, %@", url, la, lo);
        
        NSData* data = [NSData dataWithContentsOfURL:
                        url];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
        
        RenVenLabel.text = @"RENTA";
        
    }
    if([ventRentST isEqualToString:@"2"]){
        
        // NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@", la, lo]];
        
        NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@", latitudeST, longitudeST]];
        
        
        //   NSLog(@"%@, %@, %@", url, la, lo);
        
        NSData* data = [NSData dataWithContentsOfURL:
                        url];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
        
        RenVenLabel.text = @"VENTA Y RENTA";
        
    }
    if([ventRentST isEqualToString:@"3"]){
        
        NSString *urlBase = @"http://century21.elolabs.com:8888/search?";
        NSString *strData = [NSString stringWithFormat:@"q=&estado=%@&ciudad=%@&colonia=%@", self.estadoS, self.ciudadS, self.coloniaS];
        NSString *urlData = [strData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",urlBase,urlData]];
        NSData* data = [NSData dataWithContentsOfURL: url];
        ////
        //   NSData* data = [NSData dataWithContentsOfURL: url];
        
        [self performSelectorOnMainThread:@selector(fetchSearchData:)
                               withObject:data waitUntilDone:YES];
        
        
    }

    
}

- (void)viewDidLoad
{
    [self.activityIndicator startAnimating];
    [self.view bringSubviewToFront:scrollView];
    /////
    self.socialesView.alpha = 0;
   // NSLog(@"ventrent %@", ventRentST);
    
    //////
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-detalles-e-inmobiliarias.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
/////////

    UIGraphicsBeginImageContext(self.socialesView.frame.size);
    [[UIImage imageNamed:@"fondo-detalles-e-inmobiliarias.png"] drawInRect:self.socialesView.bounds];
    UIImage *imageSoc = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.socialesView.backgroundColor = [UIColor colorWithPatternImage:imageSoc];
    /////////

/*   
 UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
   // [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
//////////////
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage2 = [[UIImage imageNamed:@"boton-favoritos.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    [button2 setBackgroundImage:butImage2 forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(favoritos) forControlEvents:UIControlEventTouchUpInside];
    button2.frame = CGRectMake(0, 0, 35, 32);
    // [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2] ;
    self.navigationItem.rightBarButtonItem = backButton2;

    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Header-agente-inmobiliario.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
     [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
*/
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Header-agente-inmobiliario.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"Header-agente-inmobiliario.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    backButton.enabled = NO;
    self.navigationItem.leftBarButtonItem = backButton;
  
  //  [self.view bringSubviewToFront:self.navigationController.navigationBar];
    
 //   self.navigationItem.leftBarButtonItem.enabled = NO;
    
    
  /*  NSString *imST = [[NSString alloc]initWithFormat:@"%@", [imagesVenta objectAtIndex:[imageST intValue]]];
    
    UIGraphicsBeginImageContext (self.imageCasa.frame.size);
    [[UIImage imageNamed:imST] drawInRect:self.imageCasa.bounds];
    UIImage *iST = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    imageCasa.backgroundColor = [UIColor colorWithPatternImage:iST];
    
    NSLog(@"imSt:%@,", imageST);
   */
    
     NSString *fullST = [[NSString alloc]initWithFormat:@"%@", [fulAdress objectAtIndex:[imageST intValue]]];
  //  fulAdressLabel.text = fullST;
  //  [fullST release];
    
    NSString *prST = [[NSString alloc]initWithFormat:@"%@", [precio objectAtIndex:[imageST intValue]]];
  //  priceLabel.text = prST;
    //[prST release];
    
   
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"IMAGEst:%@", self.imageST);
    
   // [self requestAgentes];
}

-(void)requestAgentes{
////Request
NSURL *url = [[NSURL alloc]initWithString:@"http://century21.elolabs.com:8888/update_agentes"];

//   NSLog(@"%@, %@, %@", url, la, lo);

NSData* dataAg = [NSData dataWithContentsOfURL:
                url];
[self performSelectorOnMainThread:@selector(fetchedAgente:)
                       withObject:dataAg waitUntilDone:YES];

}

- (void)fetchSearchData:(NSData *)responseData {
    //parse out the json data
    // NSData *jsonData = [NSData data];
    
    //  NSString* dataStr  = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    //  NSData *jsonData = [NSData dataWithContentsOfFile:dataStr];
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSArray* latestLoans = [json objectForKey:@"propiedades"]; //2
    NSArray* keys = [[json objectForKey:@"propiedades"] allKeys];
    
    
    NSLog(@" %@,KEYS:%@ %i", json,keys, [latestLoans count]); //3
    
    //   NSString *part = [NSString stringWithFormat:@"%@",[latestLoans objectAtIndex:0] ];
    
    //  NSLog(@"%@", part);
    
    // 1) Get the latest loan
    self.images = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.tipo = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.proposito = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.adress = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.precio = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.calle = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    
    self.tipo = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.colonia = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.descripcion = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.ciudad = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.estado = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.CP = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.agentesInTS = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    
    
    for (int i =0; i < [keys count]; i++) {
        
        
        NSString *keyProp = [NSString stringWithFormat:@"%@", [keys objectAtIndex:i]];
        
        NSDictionary* propiedad = [[json objectForKey:@"propiedades"] objectForKey:keyProp];
        NSDictionary* det = [propiedad objectForKey:@"detalles"];
        
        // 2) Get the funded amount and loan amount
        NSString* tipoST = [NSString stringWithFormat:@"%@",[det objectForKey:@"tipo"] ];
        NSString* propST = [NSString stringWithFormat:@"%@",[det objectForKey:@"proposito"] ];
      //  NSString* descST = [NSString stringWithFormat:@"%@",[det objectForKey:@"descripcion"] ];
        NSString* precioST = [NSString stringWithFormat:@"%@",[det objectForKey:@"precio"] ];
        NSString* calleST = [NSString stringWithFormat:@"%@",[det objectForKey:@"calle"] ];
        //"img_list"
        
        NSString* colST = [NSString stringWithFormat:@"%@",[det objectForKey:@"colonia"] ];
        NSString* descST = [NSString stringWithFormat:@"%@",[det objectForKey:@"descripcion"] ];
        NSString* ciudadST = [NSString stringWithFormat:@"%@",[det objectForKey:@"ciudad"] ];
        NSString* estST = [NSString stringWithFormat:@"%@",[det objectForKey:@"estado"] ];
        NSString* cpST = [NSString stringWithFormat:@"%@",[det objectForKey:@"cp"] ];
        
        NSString* agINT = [NSString stringWithFormat:@"%@",[det objectForKey:@"agente"] ];
        
        self.agenteINT = agINT;
        
        NSArray *imgs = [NSArray arrayWithArray:[det objectForKey:@"img_list"]];
        //  [imgs ];
        
        NSArray *reveAr = [self reversedArray:imgs];
        
        // self.images = imgs;
        NSString *url = [NSString stringWithFormat:@"%@", [reveAr lastObject]];
        
        NSLog(@"%@", url);
        
        //    NSLog(@"imgs: %@, %i, %@", imgs, [imgs count], [imgs lastObject]);
        
      //  [self.images insertObject:url atIndex:i];
        
        [self.tipo insertObject:tipoST atIndex:i];
       [self.proposito insertObject:propST atIndex:i];
     //   [self.adress insertObject:descST atIndex:i];
        [self.precio insertObject:precioST atIndex:i];
        [self.calle insertObject:calleST atIndex:i];
        
        [self.colonia insertObject:colST atIndex:i];
        [self.descripcion insertObject:descST atIndex:i];
        [self.ciudad insertObject:ciudadST atIndex:i];
        [self.estado insertObject:estST atIndex:i];
        [self.CP insertObject:cpST atIndex:i];
         [self.agentesInTS insertObject:agINT atIndex:i];
        
        
      //  if ([self.images count]==0) {
       //     [self.images insertObject:[UIImage imageNamed:@"imagen-no-disponible-iphone4.png"] atIndex:0];
        //}
        
        NSLog(@"dict: %@", self.images);
        
        //  NSLog(@"GPS: %@, %@, %@,", latST, longST, descST);
        
    }


[self configLabelsSearch];
    [self requestAgentes];
    
}

-(void)configLabelsSearch{
        [self.activityIndicator stopAnimating];
    
    NSString *tpST = [[NSString alloc]initWithFormat:@"%@", [tipo objectAtIndex:[imageST intValue]]];
    tipoLabel.text = tpST;
    
    NSString *CLLST = [[NSString alloc]initWithFormat:@"%@", [calle objectAtIndex:[imageST intValue]]];
    calleLabel.text = CLLST;
    
    NSString *proST = [[NSString alloc]initWithFormat:@"%@", [proposito objectAtIndex:[imageST intValue]]];
    RenVenLabel.text =proST;
    
    int index = [self.imageST intValue];
    
    
    priceLabel.text = [NSString stringWithFormat:@"%@",[self.precio objectAtIndex:index] ];
  //  calleLabel.text = [NSString stringWithFormat:@"%@",[self.calle objectAtIndex:index] ];
    colLabel.text = [NSString stringWithFormat:@"%@",[self.colonia objectAtIndex:index] ];
    descLabel.text = [NSString stringWithFormat:@"%@",[self.descripcion objectAtIndex:index] ];
    ciudadLabel.text = [NSString stringWithFormat:@"%@",[self.ciudad objectAtIndex:index] ];
    estadoLabel.text = [NSString stringWithFormat:@"%@",[self.estado objectAtIndex:index] ];
    cpLabel.text = [NSString stringWithFormat:@"%@",[self.CP objectAtIndex:index] ];
    
    if ([self.images count]==0) {
        self.imagNoDisp.hidden = NO;
     //
        //self.imagNoDisp.image = [UIImage imageNamed:@"Imagen-no-disponible.png"];
    }else{
        self.imagNoDisp.image = [UIImage imageNamed:@"imagen-no-disponible-iphone4.png"];
        self.imagNoDisp.hidden = NO;
    }
}

- (void)fetchedAgente:(NSData *)responseData{
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
  //   NSArray* latestLoans = [json objectForKey:@"propiedades"]; //2
    NSArray* keys = [[json objectForKey:@"agentes"] allKeys];
  //  NSLog(@"%@", keys);
    
    //   NSLog(@"keys: %@,KEYS:%@ %i", latestLoans,keys, [latestLoans count]); //3
    
    //   NSString *part = [NSString stringWithFormat:@"%@",[latestLoans objectAtIndex:0] ];
    
    //
    
    // 1) Get the latest loan
    
    self.agentes = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    self.agentesID = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    // self.longitude = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    // self.adress = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    
    for (int i =0; i < [keys count]; i++) {
        
        
        NSString *keyProp = [NSString stringWithFormat:@"%@", [keys objectAtIndex:i]];
        
        NSDictionary* propiedad = [[json objectForKey:@"agentes"] objectForKey:keyProp];
        
        // 2) Get the funded amount and loan amount
        NSString* nameST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"nombre"] ];
        
         NSString* nameID = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"id"] ];
        
        
        [self.agentes insertObject:nameST atIndex:i];
        [self.agentesID insertObject:nameID atIndex:i];
        
        NSLog(@"%@, %@", keyProp, nameST);


}

//NSLog(@"ints%@", self.agentesInTS);

     int index = [self.imageST intValue];
    NSString *agenteSTring = [NSString stringWithFormat:@"%@", [self.agentesInTS objectAtIndex:index]];
    
  //  int agent = [agenteSTring intValue]%113;
    int agent = [self.agentesID indexOfObject:agenteSTring];
    agenteIntGen = agent;
    
    NSLog(@"%i, %@", agent, agenteSTring);
   
   // NSString* precioST = [NSString stringWithFormat:@"%@",[self.precio objectAtIndex:agent] ];
   // NSString* calleST = [NSString stringWithFormat:@"%@",[self.calle objectAtIndex:agent] ];
    NSString *agenteFINAL = [NSString stringWithFormat:@"%@", [self.agentes objectAtIndex:agent]];
    
    NSLog(@"%i, %@, %@", agent, agenteSTring, agenteFINAL);
         //calleST, precioST);
    
    self.title = agenteFINAL;
    subtitleLabel.text = agenteFINAL;
    
    
}

-(void)fetchedData:(NSData *)responseData {
    //parse out the json data
    
   
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSArray* latestLoans = [json objectForKey:@"propiedades"]; //2
    NSArray* keys = [[json objectForKey:@"propiedades"] allKeys];
    
    
       NSLog(@"keys: %@,KEYS:%@ %i", latestLoans,keys, [latestLoans count]); //3
    
    //   NSString *part = [NSString stringWithFormat:@"%@",[latestLoans objectAtIndex:0] ];
    
      NSLog(@"%@", json);
    
    // 1) Get the latest loan
    
   self.images = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.precio = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.calle = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.tipo = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.colonia = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.descripcion = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.ciudad = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.estado = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.CP = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.agentesInTS = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    
    for (int i =0; i < [latestLoans count]; i++) {
        
        
        NSString *keyProp = [NSString stringWithFormat:@"%@", [keys objectAtIndex:i]];
        
        NSDictionary* propiedad = [[json objectForKey:@"propiedades"] objectForKey:keyProp];
        
       
        NSString* precioST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"precio"] ];
        NSString* calleST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"calle"] ];
        NSString* colST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"colonia"] ];
        NSString* descST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"descripcion"] ];
        NSString* ciudadST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"ciudad"] ];
        NSString* estST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"estado"] ];
        NSString* cpST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"cp"] ];
        NSString* tipoST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"tipo"] ];

        
      //  NSString* agINT = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"agente"] ];
        
        NSArray *imgs = [NSArray arrayWithArray:[propiedad objectForKey:@"img_list"]];
        ///
        NSString *agente = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"agente"] ];
        
       // self.nombreAgente = agente;
        
        
               //    NSLog(@"imgs: %@, %i, %@", imgs, [imgs count], [imgs lastObject]);
        
        [self.images insertObject:imgs atIndex:i];

          //   NSLog(@"imgs: %@", self.images);

          [self.tipo insertObject:tipoST atIndex:i];

        [self.precio insertObject:precioST atIndex:i];
        [self.calle insertObject:calleST atIndex:i];
        [self.colonia insertObject:colST atIndex:i];
        [self.descripcion insertObject:descST atIndex:i];
        [self.ciudad insertObject:ciudadST atIndex:i];
        [self.estado insertObject:estST atIndex:i];
        [self.CP insertObject:cpST atIndex:i];
        
        [self.agentesInTS insertObject:agente atIndex:i];
        
        //   NSLog(@"dict: %@", propiedad);
        
      //  NSLog(@"try: %@, %@, %@,%@, %@, %@,%@", self.precio, self.calle, self.colonia, self.descripcion, self.ciudad, self.estado, self.CP);
        
    }
    
 //   NSLog(@"%@", self.agentesInTS);
    
    
    
   // self.agenteINT = agINT;
    
    int index = [self.imageST intValue];
    
    priceLabel.text = [NSString stringWithFormat:@"%@",[self.precio objectAtIndex:index] ];
    calleLabel.text = [NSString stringWithFormat:@"%@",[self.calle objectAtIndex:index] ];
    colLabel.text = [NSString stringWithFormat:@"%@",[self.colonia objectAtIndex:index] ];
    descLabel.text = [NSString stringWithFormat:@"%@",[self.descripcion objectAtIndex:index] ];
    ciudadLabel.text = [NSString stringWithFormat:@"%@",[self.ciudad objectAtIndex:index] ];
    estadoLabel.text = [NSString stringWithFormat:@"%@",[self.estado objectAtIndex:index] ];
    cpLabel.text = [NSString stringWithFormat:@"%@",[self.CP objectAtIndex:index] ];
   // NSString *tpST = [[NSString alloc]initWithFormat:@"%@", [self.tipo objectAtIndex:index]];
    tipoLabel.text = [NSString stringWithFormat:@"%@",[self.tipo objectAtIndex:index] ];
    //self.title = agenteSTring;

    
    
    
    ///////////////////////
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
                      CGRect scrollViewFrame = CGRectMake(0, -9, 320, 144);
            self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            CGRect scrollViewFrame = CGRectMake(0, -9, 320, 232);
            self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];        }
    }
    
    
    
    [self.view addSubview:self.scrollView];
    CGSize scrollViewContentSize = CGSizeMake(320, 400);
    [self.scrollView setContentSize:scrollViewContentSize];
       scrollView.delegate = self;
    
    [self.scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.backgroundColor = nil;
    
    NSUInteger nimages = 0;
    CGFloat cx = 0;
    
    NSArray *partIMAG =[NSArray arrayWithArray:[self.images objectAtIndex:index]];
    
    NSLog(@"PART: %@", partIMAG);
    
    for (int i =0; i < [partIMAG count] ; i++) {
        NSString *imageName = [NSString stringWithFormat:@"%@",[partIMAG objectAtIndex:i] ];
      //  UIImage *image = [UIImage imageNamed:imageName];
  //      if (image == nil) {
           // break;
    //    }
        UIImageView *imageView = [[UIImageView alloc] init];
        
        [self downloadImage:imageName inBackground:imageView];
        
        CGRect rect = imageView.frame;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                
                rect.size.height = 144
                ;
                rect.size.width = 320;
                rect.origin.x = ((scrollView.frame.size.width - (imageView.image.size.width))/2)*nimages + cx;
                rect.origin.y = 0;
               
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                
                rect.size.height = 232
                ;
                rect.size.width = 320;
                rect.origin.x = ((scrollView.frame.size.width - (imageView.image.size.width))/2)*nimages + cx;
                rect.origin.y = 0;
                
            }
        }
///Diagonal de San Antonio esq con Cempoala. Parque 
        
        
        
        imageView.frame = rect;
        
        [scrollView addSubview:imageView];
        //       [imageView release];
        
        cx += scrollView.frame.size.width;
    }
    
    
    [scrollView setContentSize:CGSizeMake(cx, [scrollView bounds].size.height)];
    
    
    NSArray *imdArr =[NSArray arrayWithArray:[self.images objectAtIndex:index]];
  //  NSString *imageName = [NSString stringWithFormat:@"%@",[imdArr objectAtIndex:0] ];
    NSLog(@"%@", imdArr);
    
    if ([imdArr count]==0) {
        self.imagNoDisp.hidden = NO;
        
         
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
               self.imagNoDisp.image = [UIImage imageNamed:@"Imagen-no-disponible-iphone4.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
               self.imagNoDisp.image = [UIImage imageNamed:@"imagen-no-disponible.png"];       }
        }

        
    }else{
        self.imagNoDisp.hidden = YES;
    }


    [self.activityIndicator stopAnimating];
    
     [self requestAgentes];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)gotoBack:(id)sender
{
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];

    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)goToMaps{
  //  NSLog(@"Go to Seven");
    
    // UIApplication *app = [UIApplication sharedApplication];;
    //  [app openURL:[NSURL URLWithString:@"http://maps.google.com/maps?q=Monterrey"]];
    
    // CLLocationCoordinate2D *currentLocation = [self getCurrentLocation];
    // this uses an address for the destination.  can use lat/long, too with %f,%f format
    NSString* address = subtitleLabel.text;
    NSString* url = [NSString stringWithFormat: @"http://maps.apple.com/maps?saddr=%f,%f&daddr=%@",
                     [latitudeST floatValue], [longitudeST floatValue],
                     [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    
    NSLog(@"%@, %@", latitudeST, longitudeST);
    
    
}

-(IBAction)shareNow{
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    
    self.socialesView.alpha = 1;
    
    self.scrollView.alpha = 0;
    
    [UIView commitAnimations];
}

-(IBAction)shareClose{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    self.scrollView.alpha = 1;
    self.socialesView.alpha = 0;
    
    [UIView commitAnimations];
}

-(IBAction)FBpost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        NSString *tst =  [NSString stringWithFormat:@"Century 21. %@, %@, %@, %@", ciudadLabel.text, colLabel.text, subtitleLabel.text, priceLabel.text];
        [controller setInitialText:tst];
        
        //Adding the URL to the facebook post value from iOS
    //    [controller addURL:[NSURL URLWithString:@"http://www.eloproducciones.com"]];
        
        //Adding the Text to the facebook post value from iOS
        //  [controller addImage:[UIImage imageNamed:@"Default@2x.png"]];
        
        
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSLog(@"UnAvailable");
    }
    
    
}
-(IBAction)TWpost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        NSString *tst =  [NSString stringWithFormat:@"Century 21. %@, %@, %@, %@", ciudadLabel.text, colLabel.text, subtitleLabel.text, priceLabel.text];

        [controller setInitialText:tst];
        
        //Adding the URL to the facebook post value from iOS
     //   [controller addURL:[NSURL URLWithString:@"http://www.eloproducciones.com"]];
        
        //Adding the Text to the facebook post value from iOS
        // [controller addImage:[UIImage imageNamed:@"icono-114x114.png"]];
        
        
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSLog(@"UnAvailable");
    }
}

-(IBAction)postMail{
    
    //   NSString *urlST = [NSString stringWithFormat:@"%@", [self.urlArray objectAtIndex:[self.receta intValue]-1]];
    //  NSString *subjST = [NSString stringWithFormat:@"%@", [self.titulosRecetas objectAtIndex:[self.receta intValue]-1]];
    
     NSString *tst =  [NSString stringWithFormat:@"Century 21. %@, %@, %@, %@", ciudadLabel.text, colLabel.text, subtitleLabel.text, priceLabel.text];
    
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    NSArray *recipients = [[NSArray alloc]initWithObjects:@"Correo", nil];
    [picker setToRecipients:recipients];
    [picker setSubject:[NSString stringWithFormat:@"Contactanos"]];
    [picker setMessageBody:[NSString stringWithFormat:@"%@", tst] isHTML:YES];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(IBAction)pushAgente{
    DetallesAgente *mp = [self.storyboard instantiateViewControllerWithIdentifier:@"DetAG"];
    mp.agente = [NSString stringWithFormat:@"%i", agenteIntGen];
    mp.procedence = @"1";
    
    self.detA = mp;
    [self.navigationController pushViewController:mp animated:YES];
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];


}

-(IBAction)favoritos{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Favoritos" message:@"¿Desea añadir esta propiedad a sus Favoritos?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"Si", nil];
    [alert show];
  //  [alert release];
    
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;{
    // the user clicked OK
    if (buttonIndex == 0)
    {
        //////
        
    }else if (buttonIndex == 1)
    {
        int index = [self.imageST intValue];
        
        Century21_HOME *favs = [self.storyboard instantiateViewControllerWithIdentifier:@"home"];
        
        NSString *ciu =[NSString stringWithFormat:@"%@", [self.ciudad objectAtIndex:index]];
        NSString *col = [NSString stringWithFormat:@"%@", [self.colonia objectAtIndex:index]];
        
        int choice = [self.images count];
        
       
        if (choice > 0) {
            
            NSArray *partIMAG =[NSArray arrayWithArray:[self.images objectAtIndex:index]];
            NSString *imageName = [NSString stringWithFormat:@"%@",[partIMAG objectAtIndex:0] ];
            favs.imageS = imageName;
            NSLog(@"%@", partIMAG);

        }else{
            favs.imageS = @"http://www.schaefersigns.com/v/vspfiles/images/articles/Century21.jpg";
        }
        
        NSString *agen = [NSString stringWithFormat:@"%@", subtitleLabel.text];
        
        NSString *prec = [NSString stringWithFormat:@"%@", [self.precio objectAtIndex:index]];
        
        
        favs.ciudadStr = ciu;
        favs.coloniaS = col;
        
        
        favs.AgenteS = agen;
        favs.precioS = prec;
        favs.FavORnot = @"1";
        // favs.scrollView = nil;
        [favs.scrollView removeFromSuperview];
        
        //  NSLog(@"PLEASEEE %@, %@", favs.ciudadS, ciu);
        
        [self.navigationController pushViewController:favs animated:YES];
        
        //  [self release];
        
        //  [favs initFavoritos:[self.imageST intValue]];
    }
}

@end
