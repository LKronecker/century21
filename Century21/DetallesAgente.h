//
//  DetallesAgente.h
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-10-09.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h> 

@interface DetallesAgente : UIViewController<MFMailComposeViewControllerDelegate>{
    UIImageView *imageAgent;
    NSArray *images;
    
    NSMutableArray *franquicia;
    NSMutableArray *direccion;
    NSMutableArray *colonia;
    NSMutableArray *CP;
    NSMutableArray *ciudad;
    NSMutableArray *estado;
    NSMutableArray *telefono;
    NSMutableArray *website;
    NSMutableArray *emails;
    
    UILabel *franLabel;
    UILabel *direLabel;
    UILabel *coloLabel;
    UILabel *cpLabel;
    UILabel *ciudLabel;
    UILabel *estaLabel;
    UILabel *teleLabel;
    UILabel *webLabel;
    
    UIView *socialesView;
    
    
}

@property (nonatomic, retain) NSString *indexAgente;
@property (nonatomic, retain) NSString *procedence;
@property (nonatomic, retain) IBOutlet UIImageView *imageAgent;
@property (nonatomic, retain) NSArray *images;

@property (nonatomic, retain) IBOutlet NSMutableArray *franquicia;
@property (nonatomic, retain) IBOutlet NSMutableArray *direccion;
@property (nonatomic, retain) IBOutlet NSMutableArray *colonia;
@property (nonatomic, retain) IBOutlet NSMutableArray *CP;
@property (nonatomic, retain) IBOutlet NSMutableArray *ciudad;
@property (nonatomic, retain) IBOutlet NSMutableArray *estado;
@property (nonatomic, retain) IBOutlet NSMutableArray *telefono;
@property (nonatomic, retain) IBOutlet NSMutableArray *website;
@property (nonatomic, retain) IBOutlet NSMutableArray *emails;

@property (nonatomic, retain) IBOutlet UILabel *franLabel;
@property (nonatomic, retain) IBOutlet UILabel *direLabel;
@property (nonatomic, retain) IBOutlet UILabel *coloLabel;
@property (nonatomic, retain) IBOutlet UILabel *cpLabel;
@property (nonatomic, retain) IBOutlet UILabel *ciudLabel;
@property (nonatomic, retain) IBOutlet UILabel *estaLabel;
@property (nonatomic, retain) IBOutlet UILabel *teleLabel;
@property (nonatomic, retain) IBOutlet UILabel *webLabel;

@property (nonatomic, strong) NSString *agente;
 @property (nonatomic, retain) IBOutlet  UIView *socialesView;
-(IBAction)pushDemo;
-(IBAction)TWpost;
-(IBAction)FBpost;
-(IBAction)mail;
-(IBAction)call;
-(IBAction)shareNow;
-(IBAction)shareClose;

@end

