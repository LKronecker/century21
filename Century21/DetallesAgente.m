//
//  DetallesAgente.m
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-10-09.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "DetallesAgente.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface DetallesAgente ()

@end

@implementation DetallesAgente
@synthesize indexAgente, imageAgent, images, franquicia, direccion, ciudad, colonia, CP, estado, telefono, website, franLabel, direLabel, ciudLabel, coloLabel, cpLabel, estaLabel, teleLabel, webLabel, socialesView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    ////Request
    NSURL *url = [[NSURL alloc]initWithString:@"http://century21.elolabs.com:8888/update_agentes"];
    
    //   NSLog(@"%@, %@, %@", url, la, lo);
    
    NSData* data = [NSData dataWithContentsOfURL:url];
    
[self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    
}


- (void)viewDidLoad
{
    
    self.socialesView.alpha = 0;
    NSLog(@"%@", self.agente);
  
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    //   [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    /////////////////////
    ///////////
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-detalles-e-inmobiliarias.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    //////////
    UIGraphicsBeginImageContext(socialesView.frame.size);
    [[UIImage imageNamed:@"fondo-compartir.png"] drawInRect:socialesView.bounds];
   // UIImage *imageSoc = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    
  //  self.socialesView.backgroundColor = [UIColor colorWithPatternImage:imageSoc];
    ////
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    // NSArray* latestLoans = [json objectForKey:@"propiedades"]; //2
    NSArray* keys = [[json objectForKey:@"agentes"] allKeys];
    NSLog(@"%@", json);
    
  //     NSLog(@"KEYS:%@ %i",keys, [keys count]); //3
    
    //   NSString *part = [NSString stringWithFormat:@"%@",[latestLoans objectAtIndex:0] ];
    
    //
    
    // 1) Get the latest loan
    
    self.franquicia = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    
    self.direccion = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    
    self.colonia = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    
    self.CP = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    
    self.ciudad = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    
    self.estado = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    
    self.telefono = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    self.website = [[NSMutableArray alloc] initWithCapacity:[keys count]];
     self.emails = [[NSMutableArray alloc] initWithCapacity:[keys count]];
    
    // self.longitude = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    // self.adress = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    
    for (int i =0; i < [keys count]; i++) {
        
        
        NSString *keyProp = [NSString stringWithFormat:@"%@", [keys objectAtIndex:i]];
        
        NSDictionary* propiedad = [[json objectForKey:@"agentes"] objectForKey:keyProp];
        
        
        
        // 2) Get the funded amount and loan amount
        NSString* nameST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"nombre"] ];
        NSString* dirST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"calle"] ];
        NSString* coloST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"colonia"] ];
        
        NSString* mailST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"email"] ];
        
                NSString* cpST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"cp"] ];
                NSString* ciudadST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"ciudad"] ];
                NSString* estST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"estado"] ];
        NSString* telST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"telefono"] ];
        NSString* webST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"url"] ];
        
        
        
        [self.franquicia insertObject:nameST atIndex:i];
        [self.emails insertObject:mailST atIndex:i];
        [self.direccion insertObject:dirST atIndex:i];
        [self.colonia insertObject:coloST atIndex:i];
        [self.CP insertObject:cpST atIndex:i];
        [self.ciudad insertObject:ciudadST atIndex:i];
        [self.estado insertObject:estST atIndex:i];
        [self.telefono insertObject:telST atIndex:i];
        [self.website insertObject:webST atIndex:i];
        
        NSLog(@"all: %@, name: %@, int: %@",json, nameST, keyProp);
        
        
    }
    int AGindex = [self.agente integerValue];
    
    NSLog(@"%i", [self.franquicia count]);
    
    self.franLabel.text = [NSString stringWithFormat:@"%@",[self.franquicia objectAtIndex:AGindex]];
    self.direLabel.text = [NSString stringWithFormat:@"%@",[self.direccion objectAtIndex:AGindex]];
    self.coloLabel.text = [NSString stringWithFormat:@"%@",[self.colonia objectAtIndex:AGindex]];
    self.cpLabel.text = [NSString stringWithFormat:@"%@",[self.CP objectAtIndex:AGindex]];
    self.ciudLabel.text = [NSString stringWithFormat:@"%@",[self.ciudad objectAtIndex:AGindex]];
    self.estaLabel.text = [NSString stringWithFormat:@"%@",[self.estado objectAtIndex:AGindex]];
    self.teleLabel.text = [NSString stringWithFormat:@"%@",[self.telefono objectAtIndex:AGindex]];
    self.webLabel.text = [NSString stringWithFormat:@"%@",[self.website objectAtIndex:AGindex]];

}
-(IBAction)pushDemo{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"DEMO Desarrollado por ELO" message:@"Esta opción aun no esta activada." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
        
}

-(IBAction)gotoBack:(id)sender
{
    
  //  self.navigationController.navigationItem.title
    
    if ([self.procedence isEqualToString:@"0"]) {
   
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Ultimo-header-2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ([self.procedence isEqualToString:@"1"]) {
        
        
        UIGraphicsBeginImageContext (CGSizeMake(320, 44));
        [[UIImage imageNamed:@"Header-agente-inmobiliario.png"] drawInRect:self.navigationController.navigationBar.bounds];
        UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
        [self.navigationController popViewControllerAnimated:YES];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)shareNow{
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    
    self.socialesView.alpha = 1;
    
    [UIView commitAnimations];
}

-(IBAction)shareClose{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    
    self.socialesView.alpha = 0;
    
    [UIView commitAnimations];
}


-(IBAction)FBpost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:@"App demo para @c21mexico Desarrollada por:"];
        
        //Adding the URL to the facebook post value from iOS
        [controller addURL:[NSURL URLWithString:@"http://www.eloproducciones.com"]];
        
        //Adding the Text to the facebook post value from iOS
        //  [controller addImage:[UIImage imageNamed:@"Default@2x.png"]];
        
        
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSLog(@"UnAvailable");
    }
    
    
}
-(IBAction)TWpost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:@"App demo para @c21mexico Desarrollada por:"];
        
        //Adding the URL to the facebook post value from iOS
        [controller addURL:[NSURL URLWithString:@"http://www.eloproducciones.com"]];
        
        //Adding the Text to the facebook post value from iOS
        // [controller addImage:[UIImage imageNamed:@"icono-114x114.png"]];
        
        
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSLog(@"UnAvailable");
    }
}

-(IBAction)mail{
        NSString *url = [NSString stringWithFormat: @"mailto:agebte@mail.com?&subject=&body="];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}
-(IBAction)call{
    NSString *direc = [NSString stringWithFormat:@"%@", [self.telefono objectAtIndex:[self.agente integerValue]]];
    NSLog(@"direc1: %@", direc);
    
    NSString *TEL =[direc substringToIndex:14];
    NSString *format1 = [TEL stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString *format2 = [format1 stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *format3 = [format2 stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *format4 = [format3 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"direc2: %@", format4);
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
         
      //  NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel:%@", direc]] ;
    NSURL* callUrl=[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",format4]];
    
   // NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:80001212"]
    
        [[UIApplication sharedApplication] openURL:callUrl];
    
    
   // NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:078"];
   // [[UIApplication sharedApplication] openURL:phoneNumberURL];
    
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Esta función solo está disponible en el iPhone." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
       // [Notpermitted release];
    }
   

}

-(IBAction)postMail{
    
    //   NSString *urlST = [NSString stringWithFormat:@"%@", [self.urlArray objectAtIndex:[self.receta intValue]-1]];
    //  NSString *subjST = [NSString stringWithFormat:@"%@", [self.titulosRecetas objectAtIndex:[self.receta intValue]-1]];
    
    NSString *mail = [NSString stringWithFormat:@"%@",[self.emails objectAtIndex:[self.agente integerValue]]];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    NSArray *recipients = [[NSArray alloc]initWithObjects:mail, nil];
    [picker setToRecipients:recipients];
    
    [picker setSubject:[NSString stringWithFormat:@"..."]];
    [picker setMessageBody:[NSString stringWithFormat:@""] isHTML:YES];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
