//
//  DisplayTag.h
//  GeoLoK
//
//  Created by Leopoldo G Vargas on 2012-10-24.
//  Copyright (c) 2012 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKAnnotation.h>

@interface DisplayTag : NSObject <MKAnnotation> {
    
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
  //  MKPinAnnotationColor *pinColor;
}
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
// @property (nonatomic, copy)   UIColor *pinColor;


@end
