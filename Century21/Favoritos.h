//
//  Favoritos.h
//  Century21
//
//  Created by Leopoldo G Vargas on 2013-11-21.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Century21 HOME.h"

@class Century21_HOME;

@interface Favoritos : NSObject{
    
    Century21_HOME *home;
    
    NSMutableArray *imagesVenta;
    NSMutableArray *fulAdress;
    NSMutableArray *precio;
    NSMutableArray *calle;
    NSMutableArray *colonia;
    NSMutableArray *descripcion;
    NSMutableArray *tipo;
    NSMutableArray *ciudad;
    NSMutableArray *estado;
    NSMutableArray *CP;
    NSMutableArray *images;
    NSMutableArray *proposito;
    NSMutableArray *adress;
    NSMutableArray *agentes;


}

@property (nonatomic, retain)  NSMutableArray *images;
@property (nonatomic, retain) NSMutableArray *imagesVenta;
@property (nonatomic, retain) NSMutableArray *fulAdress;
@property (nonatomic, retain) NSMutableArray *precio;
@property (nonatomic, retain)  NSMutableArray *calle;
@property (nonatomic, retain)  NSMutableArray *colonia;
@property (nonatomic, retain)  NSMutableArray *descripcion;
@property (nonatomic, retain)  NSMutableArray *tipo;
@property (nonatomic, retain)  NSMutableArray *ciudad;
@property (nonatomic, retain)  NSMutableArray *estado;
@property (nonatomic, retain)  NSMutableArray *CP;
@property (nonatomic, retain)  NSMutableArray *proposito;
@property (nonatomic, retain) NSMutableArray *agentes;
@property (nonatomic, retain) NSMutableArray *adress;

@property  (strong,nonatomic) NSString *ciudadST;
@property  (strong,nonatomic) NSString *coloniaST;

-(void)initFavoritos:(int)index;
-(NSArray *)returnFavorito:(int)index;

@property (nonatomic, retain) Century21_HOME *home;

@end
