//
//  GPSMapViewController.h
//  Century21
//
//  Created by Alan MB on 09/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapAnotations.h"
#import "Detalles.h"
#import "MapCell.h"
#import "Century21 HOME.h"

@class Detalles, Century21_HOME;

@interface GPSMapViewController : UIViewController  <MKMapViewDelegate, UITabBarDelegate, UITableViewDelegate>{
    
    UIView *slide;
    MKMapView *mapView;
    
    NSMutableArray *adress;
    NSMutableArray *latitude;
    NSMutableArray *longitude;
    NSMutableArray *agenciaVentas;
    NSMutableArray *precio;
     NSMutableArray *calle;
    
    
    Detalles *detalles;
    Century21_HOME *home;
    
    UITabBar *tabbar;
    UITableView *listHouses;
    
    UITextField *ciudadText;
    UITextField *catText;
    UITextField *rangoText;
    
    bool listMapsCount;
    
    BOOL slideControl;
    
    
    NSMutableArray *images;
    
    UIActivityIndicatorView *activityIndicator;
    
   
    
    
}
@property (nonatomic, retain) IBOutlet  UIView *slide;
@property (nonatomic, retain) IBOutlet UITableView *listHouses;
@property (nonatomic, retain) IBOutlet UITabBar *tabbar;
@property (nonatomic, retain) NSString *MapString;
@property (nonatomic, retain) MKMapView *mapView;
@property (nonatomic, retain)   Detalles *detalles;
@property (nonatomic, retain)  Century21_HOME *home;
@property (nonatomic, retain) NSMutableArray *adress;
@property (nonatomic, retain) NSMutableArray *latitude;
@property (nonatomic, retain) NSMutableArray *longitude;
@property (nonatomic, retain) NSMutableArray *agenciaVentas;

@property (nonatomic, retain) NSMutableArray *precio;
@property (nonatomic, retain) NSMutableArray *calle;

@property (nonatomic, retain)  NSMutableArray *images;


@property (nonatomic, retain) IBOutlet UITextField *ciudadText;
@property (nonatomic, retain) IBOutlet UITextField *catText;
@property (nonatomic, retain) IBOutlet UITextField *rangoText;

@property  (strong,nonatomic) NSString *searchURL;
@property  (strong,nonatomic) NSString *estadoST;
@property  (strong,nonatomic) NSString *ciudadST;
@property  (strong,nonatomic) NSString *coloniaST;

   @property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;




-(IBAction)showDetails:(id)sender;
-(IBAction)places:(id)sender;
-(IBAction)getOptions:(UISwipeGestureRecognizer *)recognizer;
- (IBAction) tapBackground: (id) sender;
-(IBAction)advSearch;

@end
