//
//  GPSMapViewController.m
//  Century21
//
//  Created by Alan MB on 09/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "GPSMapViewController.h"
#import "DisplayTag.h"
#import "MapAnotations.h"



@interface GPSMapViewController ()

@end

@implementation GPSMapViewController
@synthesize mapView, adress, longitude, latitude, MapString, tabbar, agenciaVentas, listHouses, detalles, slide, images, ciudadText, catText, rangoText, precio, calle, activityIndicator, home;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //  [listHouses initWithFrame:listHouses.frame style:UITableViewCellStyleSubtitle];
        
    }
    return self;
}

- (void)downloadImage:(NSString *)inputURL inBackground:(UIImageView *)imageView
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(imageView.bounds.size.width/2, imageView.bounds.size.height/2)];
    [imageView addSubview:activity];
    [activity startAnimating];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",inputURL]];

    
    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
    dispatch_async(download_queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *_image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            [activity removeFromSuperview];
            [imageView setImage:_image];
        });
    });
    // dispatch_release(download_queue);
}

- (NSArray *)reverseMutArray:(NSMutableArray *)inputArray {
    if ([inputArray count] == 0)
        return inputArray;
    NSUInteger i = 0;
    NSUInteger j = [inputArray count] - 1;
    while (i < j) {
        [inputArray exchangeObjectAtIndex:i
                  withObjectAtIndex:j];
        
        i++;
        j--;
    }
    return inputArray;
}

- (NSArray *)reversedArray:(NSArray *)inputArray  {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[inputArray count]];
    NSEnumerator *enumerator = [inputArray reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    return array;
}


///Direccion de coorp: Carretera A Chankanaab Km 6.5. Cozumel Quintana roo. CP. 77600. Mexico.

-(void)viewDidAppear:(BOOL)animated{
    
   //  [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(JSONrequest) userInfo:nil repeats:NO];
    [self JSONrequest];
    
    

}

- (void)viewDidLoad
{
    [self.activityIndicator startAnimating];
    ////
    [self.view bringSubviewToFront:tabbar];
    
    UISwipeGestureRecognizer *recognizer;
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(getOptions:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionUp| UISwipeGestureRecognizerDirectionDown)];
    [[self slide] addGestureRecognizer:recognizer];
   // [recognizer release];
    
    
    // [self.view reloadInputViews];
    slideControl = 0;
    
    
    UIGraphicsBeginImageContext(self.slide.frame.size);
    [[UIImage imageNamed:@"Slide (1).png"] drawInRect:self.slide.bounds];
 //   UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  
    
    [tabbar setSelectedItem:[tabbar.items objectAtIndex:0]];
    // tabbar.selectedItem.enabled = NO;
    
    listHouses.alpha=0;
    listMapsCount =0;
    ////////
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    //  [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    
    // self.tabbar.items
    tabbar.delegate =self;
    // [tabbar ];
    self.tabBarItem.title = @"Mapas";
    
  //  
    
    
    mapView = [[MKMapView alloc]initWithFrame:self.view.bounds];
	[self.view insertSubview:mapView atIndex:0];
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    
    //  CLLocation *userLoc = mapView.userLocation.location;
    //   CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    MKCoordinateRegion mexCity = { {0.0, 0.0 }, { 0.0, 0.0 } };
    
    mexCity.span.longitudeDelta = 11.0f;
    mexCity.span.latitudeDelta = 11.0f;
    mexCity.center.latitude = 19.454060 ;
    mexCity.center.longitude = -99.180288;
    
    [mapView setRegion:mexCity animated:YES];
    
    [mapView setDelegate:self];
    
    mapView.showsUserLocation = YES;
    /////
    
    
    
    [super viewDidLoad];
    
   
    
    // UITableView *table = [[UITableView alloc]initWithFrame:listHouses.frame style:UITableViewCellStyleValue1];
    //   self.listHouses =table;
    // Do any additional setup after loading the view from its nib.
    
    [self.view bringSubviewToFront:slide];
}
- (void)JSONrequest {

    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    NSString *la = [[NSString alloc]initWithFormat:@"%f", userCoordinate.latitude];
    NSString *lo = [[NSString alloc]initWithFormat:@"%f", userCoordinate.longitude];
    
    
    if([MapString isEqualToString:@"0"]){
        
        NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@&tipo=venta", la, lo]];
        
        //  NSURL *url = [[NSURL alloc]initWithString:@"http://century21.elolabs.com:8888/lat=19.397367&long=-99.075976&tipo=venta"];
        
        NSLog(@"%@, %@, %@", url, la, lo);
        
        NSData* data = [NSData dataWithContentsOfURL:
                        url];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
        
        NSLog(@"%@", data);
        
        
    }
    if([MapString isEqualToString:@"1"]){
        
        NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@&tipo=renta", la, lo]];
        
        // NSURL *url = [[NSURL alloc]initWithString:@"http://century21.elolabs.com:8888/lat=19.397367&long=-99.075976&tipo=renta"];
        
        NSLog(@"%@, %@, %@", url, la, lo);
        
        NSData* data = [NSData dataWithContentsOfURL:
                        url];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
        
        
    }
    if([MapString isEqualToString:@"2"]){
        
        NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/lat=%@&long=%@", la, lo]];
        
        // NSURL *url = [[NSURL alloc]initWithString:@"http://century21.elolabs.com:8888/lat=19.397367&long=-99.075976"];
        
        
        NSLog(@"%@, %@, %@", url, la, lo);
        
        NSData* data = [NSData dataWithContentsOfURL:
                        url];
        
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
        
        
    }
    
    if([MapString isEqualToString:@"3"]){
        
  /*      NSLog(@"%@", self.searchURL);
        NSString *urlST = [NSString stringWithFormat:@"http://century21.elolabs.com:8888/search?q=&estado=%@&ciudad=%@&colonia=%@", self.estadoST, self.ciudadST, self.coloniaST];
        
        urlST = [urlST stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
         NSLog(@"urlST:%@", urlST);
        
        NSURL *url = [[NSURL alloc]initWithString:urlST];
   */     
        //NSURL *url = [[NSURL alloc]initWithString:@"http://century21.elolabs.com:8888/search?q=&estado=Ciudad%20de%20M%C3%A9xico,%20Distrito%20Federal%20(DF)&ciudad=Alvaro%20Obreg%C3%B3n&colonia=Santa%20Fe"];
        
    ////
        NSString *urlBase = @"http://century21.elolabs.com:8888/search?";
        NSString *strData = [NSString stringWithFormat:@"q=&estado=%@&ciudad=%@&colonia=%@", self.estadoST, self.ciudadST, self.coloniaST];
        NSString *urlData = [strData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",urlBase,urlData]];
        NSData* data = [NSData dataWithContentsOfURL: url];
        ////
     //   NSData* data = [NSData dataWithContentsOfURL: url];
        
        [self performSelectorOnMainThread:@selector(fetchSearchData:)
                               withObject:data waitUntilDone:YES];
        
        
    }
    
    

}
- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
   // NSData *jsonData = [NSData data];
    
  //  NSString* dataStr  = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
  //  NSData *jsonData = [NSData dataWithContentsOfFile:dataStr];
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSArray* latestLoans = [json objectForKey:@"propiedades"]; //2
    NSArray* keys = [[json objectForKey:@"propiedades"] allKeys];
    
    
    NSLog(@" %@,KEYS:%@ %i", json,keys, [latestLoans count]); //3
    
    //   NSString *part = [NSString stringWithFormat:@"%@",[latestLoans objectAtIndex:0] ];
    
    //  NSLog(@"%@", part);
    
    // 1) Get the latest loan
    self.images = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.latitude = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.longitude = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.adress = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.precio = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.calle = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    
    
    for (int i =0; i < [latestLoans count]; i++) {
    
        
        NSString *keyProp = [NSString stringWithFormat:@"%@", [keys objectAtIndex:i]];
        
        NSDictionary* propiedad = [[json objectForKey:@"propiedades"] objectForKey:keyProp];
        
        // 2) Get the funded amount and loan amount
        NSString* latST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"lat"] ];
        NSString* longST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"long"] ];
        NSString* descST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"descripcion"] ];
         NSString* precioST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"precio"] ];
         NSString* calleST = [NSString stringWithFormat:@"%@",[propiedad objectForKey:@"calle"] ];
        //"img_list"
        NSArray *imgs = [NSArray arrayWithArray:[propiedad objectForKey:@"img_list"]];
      //  [imgs ];
        
        NSArray *reveAr = [self reversedArray:imgs];
        
       // self.images = imgs;
        NSString *url = [NSString stringWithFormat:@"%@", [reveAr lastObject]];
        
        NSLog(@"%@", url);
        
     //    NSLog(@"imgs: %@, %i, %@", imgs, [imgs count], [imgs lastObject]);
        
        [self.images insertObject:url atIndex:i];
        
        [self.latitude insertObject:latST atIndex:i];
        [self.longitude insertObject:longST atIndex:i];
        [self.adress insertObject:descST atIndex:i];
        [self.precio insertObject:precioST atIndex:i];
        [self.calle insertObject:calleST atIndex:i];
        
           NSLog(@"dict: %@", propiedad);
        
      //  NSLog(@"GPS: %@, %@, %@,", latST, longST, descST);
        
    }
    
      NSLog(@"imgs: %@, %i, %@", self.images, [self.images count], [self.images lastObject]);
    
   

    [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(places:) userInfo:nil repeats:NO];
    
   //  [self.activityIndicator stopAnimating];

}
- (void)fetchSearchData:(NSData *)responseData {
    //parse out the json data
    // NSData *jsonData = [NSData data];
    
    //  NSString* dataStr  = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    //  NSData *jsonData = [NSData dataWithContentsOfFile:dataStr];
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSArray* latestLoans = [json objectForKey:@"propiedades"]; //2
    NSArray* keys = [[json objectForKey:@"propiedades"] allKeys];
    
    
    NSLog(@" %@,KEYS:%@ %i", json,keys, [latestLoans count]); //3
    
    //   NSString *part = [NSString stringWithFormat:@"%@",[latestLoans objectAtIndex:0] ];
    
    //  NSLog(@"%@", part);
    
    // 1) Get the latest loan
    self.images = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.latitude = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.longitude = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.adress = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.precio = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    self.calle = [[NSMutableArray alloc] initWithCapacity:[latestLoans count]];
    
    
    for (int i =0; i < [keys count]; i++) {
        
        
        NSString *keyProp = [NSString stringWithFormat:@"%@", [keys objectAtIndex:i]];
        
        NSDictionary* propiedad = [[json objectForKey:@"propiedades"] objectForKey:keyProp];
                NSDictionary* det = [propiedad objectForKey:@"detalles"];
        
        // 2) Get the funded amount and loan amount
        NSString* latST = [NSString stringWithFormat:@"%@",[det objectForKey:@"lat"] ];
        NSString* longST = [NSString stringWithFormat:@"%@",[det objectForKey:@"long"] ];
        NSString* descST = [NSString stringWithFormat:@"%@",[det objectForKey:@"descripcion"] ];
        NSString* precioST = [NSString stringWithFormat:@"%@",[det objectForKey:@"precio"] ];
        NSString* calleST = [NSString stringWithFormat:@"%@",[det objectForKey:@"calle"] ];
        //"img_list"
        NSArray *imgs = [NSArray arrayWithArray:[propiedad objectForKey:@"img_list"]];
        //  [imgs ];
        
        NSArray *reveAr = [self reversedArray:imgs];
        
        // self.images = imgs;
        NSString *url = [NSString stringWithFormat:@"%@", [reveAr lastObject]];
        
        NSLog(@"%@", url);
        
        //    NSLog(@"imgs: %@, %i, %@", imgs, [imgs count], [imgs lastObject]);
        
        [self.images insertObject:url atIndex:i];
        
        [self.latitude insertObject:latST atIndex:i];
        [self.longitude insertObject:longST atIndex:i];
        [self.adress insertObject:descST atIndex:i];
        [self.precio insertObject:precioST atIndex:i];
        [self.calle insertObject:calleST atIndex:i];
        
        NSLog(@"dict: %@", propiedad);
        
        //  NSLog(@"GPS: %@, %@, %@,", latST, longST, descST);
        
    }
    

    [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(places:) userInfo:nil repeats:NO];
    
         
    
}



-(IBAction)places:(id)sender{
 [self.activityIndicator stopAnimating];
    //self.activityIndicator.hidden = YES;
    
    [listHouses reloadData];
    [listHouses reloadInputViews];
    
    	mapView.delegate=self;
    
	NSMutableArray* annotations=[[NSMutableArray alloc] initWithCapacity:[self.latitude count]];
    
    for (int i=0; i<[self.latitude count]; i++) {
        float lat = [[latitude objectAtIndex:i] floatValue];
        float lon = [[longitude objectAtIndex:i] floatValue];
        NSString *adressString = [[NSString alloc]initWithFormat:@"%@", [calle objectAtIndex:i]];
        NSString *precioString = [[NSString alloc]initWithFormat:@"%@",[precio objectAtIndex:i]];
        
        CLLocationCoordinate2D theCoordinate;
        theCoordinate.latitude = lat;
        theCoordinate.longitude = lon;
        
        MapAnotations* myAnnotation1=[[MapAnotations alloc] init];
        
        myAnnotation1.coordinate=theCoordinate;
        myAnnotation1.title=adressString;
        myAnnotation1.subtitle=precioString;
        //  myAnnotation1.pinColor = MKPinAnnotationColorPurple;
        
       // myAnnotation1.image = [UIImage imageNamed:@"pin_image.png"];
      //  myAnnotation1.
        
        [mapView addAnnotation:myAnnotation1];
        [annotations insertObject:myAnnotation1 atIndex:i];
        
    }
    
	
	//NSLog(@"ANNOT:%@ num:%d",annotations,[annotations count]);
    
    
    if ([MapString isEqualToString:@"3"]) {
         [self gotoLocation2];
    }else{
    [self gotoLocation];
    }
    
}

- (void)gotoLocation
{
    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    
   // newRegion.center.latitude = 19.38164;
   // newRegion.center.longitude = -99.179174;
    
    newRegion.span.latitudeDelta = 0.03f;
    newRegion.span.longitudeDelta = 0.03f;
	
    [self.mapView setRegion:newRegion animated:YES];
}

- (void)gotoLocation2
{
  //  CLLocation *userLoc = mapView.userLocation.location;
//    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    NSString *lat = [NSString stringWithFormat:@"%@", [self.latitude lastObject]];
        NSString *lon = [NSString stringWithFormat:@"%@", [self.longitude lastObject]];
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = [lat floatValue];
    newRegion.center.longitude = [lon floatValue];
    
    newRegion.span.latitudeDelta = 10.03f;
    newRegion.span.longitudeDelta = 10.03f;
    
    // newRegion.center.latitude = 19.38164;
    // newRegion.center.longitude = -99.179174;
    
   
	
    [self.mapView setRegion:newRegion animated:YES];
}
-(IBAction)gotoBack:(id)sender
{
       UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"Ultimo-header-pagina-principal.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)getOptions:(UISwipeGestureRecognizer *)recognizer{
    if (slideControl == 0) {
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        slide.alpha = 1.0;
        CGRect slideFrame = slide.frame;
        slideFrame.origin.y += 115;
        slide.frame = slideFrame;
        [UIView commitAnimations];
        
        slideControl = 1;
    }
    else{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        slide.alpha = 1.0;
        CGRect slideFrame = slide.frame;
        slideFrame.origin.y -= 115;
        slide.frame = slideFrame;
        [UIView commitAnimations];
        
        slideControl = 0;
    }
}
- (IBAction) tapBackground: (id) sender{
	[catText resignFirstResponder];
	[rangoText resignFirstResponder];
	[ciudadText resignFirstResponder];
	
}

-(IBAction)advSearch{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"DEMO Desarrollado por ELO" message:@"Esta opción aun no esta activada." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
  
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    slideControl = 0;
    
}

#pragma mark MKMapViewDelegate
/*
 - (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
 {
 return [kml viewForOverlay:overlay];
 }
 */
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	//NSLog(@"welcome into the map view annotation");
	
	// if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	// try to dequeue an existing pin view first
	static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
	MKPinAnnotationView* pinView = [[MKPinAnnotationView alloc]
									 initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] ;
    
	//UIImage *sevenLogo = [[UIImage alloc]initWithContentsOfFile:@"7-simbolo-30x30"];
    // UIColor *sevenLogo = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"7-simbolo-30x30.png"]];
    pinView.animatesDrop=YES;
	pinView.canShowCallout=YES;
    if ([MapString isEqualToString:@"0"]) pinView.pinColor=MKPinAnnotationColorRed;
    if ([MapString isEqualToString:@"1"]) pinView.pinColor=MKPinAnnotationColorPurple;
    if ([MapString isEqualToString:@"2"]) pinView.pinColor=MKPinAnnotationColorGreen;
    
    
    
 //   MapAnotations *an = [[self.mapView selectedAnnotations] objectAtIndex:0];
    
  //  NSString *imST = [[NSString alloc]initWithFormat:@"%@",an.title];
   // int index = [calle indexOfObject:imST];
	
	
	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	[rightButton setTitle:annotation.title forState:UIControlStateNormal];
	[rightButton addTarget:self
					action:@selector(showDetails:)
		  forControlEvents:UIControlEventTouchUpInside];
	pinView.rightCalloutAccessoryView = rightButton;
    
    
    
     NSString *urlNAME = [[NSString alloc]initWithFormat:@"%@",[self.images objectAtIndex:0]];
	
	UIImageView *profileIconView = [[UIImageView alloc] init];
	[self downloadImage:urlNAME inBackground:profileIconView ];
    pinView.leftCalloutAccessoryView = profileIconView;
	
	
	
	return pinView;
}

/*
 - (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view
 calloutAccessoryControlTapped:(UIControl *)control
 {
 id<MKAnnotation> selectedAnn = view.annotation;
 
 if (selectedAnn isKindOfClass[SevenDetails class]] )
 {
 SevenDetails *vma = (SevenDetails *)selectedAnn;
 NSLog(@"selected VMA = %@, blobkey=%@", vma, vma.labelText);
 }
 else
 {
 NSLog(@"selected annotation (not a VMA) = %@", selectedAnn);
 }
 
 //do something with the selected annotation...
 }
 */

-(IBAction)showDetails:(id)sender{
    
	//NSLog(@"Annotation Click");
    
    //To be safe, may want to check that array has at least one item first.
    
//    id<MKAnnotation> ann = [[mapView selectedAnnotations] objectAtIndex:0];
    
    // OR if you have custom annotation class with other properties...
    // (in this case may also want to check class of object first)
    
    MapAnotations *an = [[mapView selectedAnnotations] objectAtIndex:0];
    
  //  NSLog(@"ann.title = %@", an.title);
    //  NSLog(@"ann.coord = %@", an.coordinate);
    
    ////////
    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    NSString *latST = [NSString stringWithFormat:@"%f", userCoordinate.latitude];
    NSString *longST = [NSString stringWithFormat:@"%f", userCoordinate.longitude];
    
    NSString *imST = [[NSString alloc]initWithFormat:@"%@",an.title];
    int index = [calle indexOfObject:imST];
    
    NSLog(@"%i", index);
    
    
 //   map.MapString = @"2";
   // [self.navigationController pushViewController:map animated:YES];

    
  //  Detalles *det = [[Detalles alloc]initWithNibName:@"Detalles" bundle:[NSBundle mainBundle]];
    
    Detalles *det = [self.storyboard instantiateViewControllerWithIdentifier:@"Det"];
    det.ventRentST = MapString;
    det.labelText = an.title;
    det.subtittleString = an.subtitle;
    
    det.latitudeST = latST;
    det.longitudeST =longST;
    
    det.estadoS = self.estadoST;
    det.ciudadS = self.ciudadST;
    det.coloniaS = self.coloniaST;
    
    // det.imageCasa = imST;
    
    NSString *contSt = [[NSString alloc]initWithFormat:@"%i",index];
    det.imageST = contSt;
   // NSLog(@"%i, %@",index, contSt);
    
    
    self.detalles = det;
    [self.navigationController pushViewController:det animated:YES];
   }

#pragma mark UITabBarDelegate

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    // NSLog(@"rawr");
    if (item.tag == 0) {
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        listHouses.alpha = 0;
        [UIView commitAnimations];
    }
    if (item.tag == 1) {
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        listHouses.alpha = 1;
        [UIView commitAnimations];
        
        [listHouses reloadData];
        [listHouses reloadInputViews];
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController willBeginCustomizingViewControllers:(NSArray *)viewControllers
{
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (1);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [adress count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // if (tableView ==self.listHouses) {
    MapCell *cell;
    static NSString *CellIdentifier = @"Cell";
    
    cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"recuadro-lista.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    //  UIGraphicsBeginImageContext(self.listHouses.ce.frame.size);  Helvetica Oblique 15.0
    //  [[UIImage imageNamed:@"recuadro-lista.png"] drawInRect:self.view.bounds];
    //  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //  UIGraphicsEndImageContext();
    
    
    // cell.textLabel.backgroundColor = [UIColor colorWithPatternImage:image ];
       //
    cell.tittleLabel.text = [calle objectAtIndex:indexPath.row];
    cell.subTittle.text = [precio objectAtIndex:indexPath.row];

    
       ////
    NSString *name = [[NSString alloc]initWithFormat:@"Imagen-cargando.png"];
    UIImage *image = [UIImage imageNamed:name];
    cell.cellImageView.image = image;

    /////
    
    NSUInteger row = [indexPath row];
    
    NSString *urlNAME = [[NSString alloc]initWithFormat:@"%@",[self.images objectAtIndex:row]];
    if ([urlNAME isEqualToString:@"(null)"]) {
        NSString *name = [[NSString alloc]initWithFormat:@"Imagen-no-disponible.png"];
        UIImage *image = [UIImage imageNamed:name];
        cell.cellImageView.image = image;
    }else{
    
    [self downloadImage:urlNAME inBackground:cell.cellImageView];
         
    }
    
        return cell;
   // }
   // return cell;
    }
    

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
/*    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
       NSString *im = [[NSString alloc]initWithFormat:@"%@",cell.textLabel.text];
    int row = [calle indexOfObject:im];
 */
    
    int row = indexPath.row;
    
    
  
     Detalles *det = [self.storyboard instantiateViewControllerWithIdentifier:@"Det"];
    
    NSString *imST = [[NSString alloc]initWithFormat:@"%i",row];
    det.imageST = imST;
    
    NSString *adressST = [[NSString alloc]initWithFormat:@"%@",[self.adress objectAtIndex:row]];
    det.labelText = adressST;
    
    NSString *subST = [[NSString alloc]initWithFormat:@"%@",[self.agenciaVentas objectAtIndex:row]];
    det.subtittleString = subST;
    
    det.estadoS = self.estadoST;
    det.ciudadS = self.ciudadST;
    det.coloniaS = self.coloniaST;
    
    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    NSString *latST = [NSString stringWithFormat:@"%f", userCoordinate.latitude];
    NSString *longST = [NSString stringWithFormat:@"%f", userCoordinate.longitude];
    
  //  NSString *latST = [[NSString alloc]initWithFormat:@"%@",[self.latitude objectAtIndex:row]];
    det.latitudeST = latST;
    
    //NSString *longST = [[NSString alloc]initWithFormat:@"%@",[self.longitude objectAtIndex:row]];
    
    det.longitudeST =longST;
    
    det.ventRentST = MapString;
    
    
    
    
    self.detalles = det;
    [self.navigationController pushViewController:det animated:YES];
    
    NSLog(@"%i", row);
   }



@end
