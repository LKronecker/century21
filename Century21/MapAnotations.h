//
//  MapAnotations.h
//  GeoLoK
//
//  Created by Leopoldo G Vargas on 2012-12-23.
//  Copyright (c) 2012 Leopoldo G Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnotations : NSObject<MKAnnotation> {
	
	CLLocationCoordinate2D	coordinate;
	NSString*				title;
     MKPinAnnotationColor* pinColor;

}

@property (nonatomic, assign)	CLLocationCoordinate2D	coordinate;
@property (nonatomic, copy)		NSString*				title;
@property (nonatomic, copy)		NSString*				subtitle;

@property (nonatomic, assign)	 MKPinAnnotationColor* pinColor;
@end
