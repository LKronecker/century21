//
//  MapAnotations.m
//  GeoLoK
//
//  Created by Leopoldo G Vargas on 2012-12-23.
//  Copyright (c) 2012 Leopoldo G Vargas. All rights reserved.
//

#import "MapAnotations.h"

@implementation MapAnotations

@synthesize title;
@synthesize subtitle;
@synthesize coordinate, pinColor;

- (void)dealloc
{
	//[super dealloc];
	self.title = nil;
	self.subtitle = nil;
  //  self.pinColor = nil;
}

@end
