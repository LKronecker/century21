//
//  ITSMapOfficeCell.h
//  SAT
//
//  Created by SATMovil on 17/07/13.
//  Copyright (c) 2013 SATMovil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapCell : UITableViewCell{
    UILabel *tittleLabel;
     UILabel *subTittle;
       UIImageView *cellImageView;

}
@property (nonatomic, retain) IBOutlet UILabel *tittleLabel;
@property (nonatomic, retain) IBOutlet UILabel *subTittle;
@property (nonatomic, retain) IBOutlet   UIImageView *cellImageView;



@end
