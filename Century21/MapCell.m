//
//  ITSMapOfficeCell.m
//  SAT
//
//  Created by SATMovil on 17/07/13.
//  Copyright (c) 2013 SATMovil. All rights reserved.
//

#import "MapCell.h"

@implementation MapCell

@synthesize cellImageView, tittleLabel, subTittle;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
