//
//  MiPropiedadViewController.h
//  Century21
//
//  Created by Alan MB on 09/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"
#import "ARCMacros.h"
#import <MessageUI/MessageUI.h> 

@interface MiPropiedadViewController : UIViewController <UIScrollViewDelegate, FPPopoverControllerDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    BOOL scrollCount;
    UIScrollView *scroll;
    FPPopoverController *popover;
    UIButton *boton;
    
    FPPopoverController *popover2;
    UIButton *boton2;
    
    FPPopoverController *popover3;
    UIButton *boton3;
    
    UIView *searchView;
    
    NSArray *estadosSearch;
    UIPickerView * estadosPicker;
    int searchControl;
    
    NSArray *deseaSearch;
    
    NSArray *tipoSearch;
    

}

@property (nonatomic, retain) IBOutlet UIScrollView *scroll;
@property (nonatomic, retain) IBOutlet UIButton *boton;
@property (nonatomic, retain) IBOutlet UIButton *boton2;
@property (nonatomic, retain) IBOutlet UIButton *boton3;

@property (nonatomic, strong) IBOutlet UITextField * nombre;
@property (nonatomic, strong) IBOutlet UITextField * email;
@property (nonatomic, strong) IBOutlet UITextField * telefono;
@property (nonatomic, strong) IBOutlet UITextField * calle;
@property (nonatomic, strong) IBOutlet UITextField * noExt;
@property (nonatomic, strong) IBOutlet UITextField * noInt;
@property (nonatomic, strong) IBOutlet UITextField * colonia;


@property (nonatomic, strong) IBOutlet UITextField * CP;
@property (nonatomic, strong) IBOutlet UITextField * ciudad;
@property (nonatomic, strong) IBOutlet UITextField * m2terreno;
@property (nonatomic, strong) IBOutlet UITextField * m2construccion;
@property (nonatomic, strong) IBOutlet UITextField * recamaras;
@property (nonatomic, strong) IBOutlet UITextField * banos;
@property (nonatomic, strong) IBOutlet UITextField * antiguedad;
@property (nonatomic, strong) IBOutlet UITextField * estacionamientos;
@property (nonatomic, strong) IBOutlet UITextField * precioEst;

@property (nonatomic, strong) IBOutlet UITextView *comentarios;
@property (nonatomic, retain) IBOutlet  UIView *searchView;

@property (nonatomic, retain) NSArray *estadosSearch;
@property (nonatomic, retain) NSArray *deseaSearch;
@property (nonatomic, retain) NSArray *tipoSearch;
 @property (nonatomic, retain) IBOutlet   UIPickerView * estadosPicker;



-(IBAction)Click:(id)sender;
-(void)selectedTableRow:(NSUInteger)rowNum;

-(IBAction)Click2:(id)sender;
-(void)selectedTableRow2:(NSUInteger)rowNum;

-(IBAction)Click3:(id)sender;
-(void)selectedTableRow3:(NSUInteger)rowNum;

- (IBAction) dissmissKeyB: (id) sender;
- (IBAction) tapBackground: (id) sender;

-(IBAction)enviar;
-(IBAction)borrar;

-(IBAction)pushPicker;
//-(IBAction)returnPicker;

-(IBAction)estados;
-(IBAction)tipo;
-(IBAction)desea;
@end
