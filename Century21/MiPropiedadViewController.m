//
//  MiPropiedadViewController.m
//  Century21
//
//  Created by Alan MB on 09/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "MiPropiedadViewController.h"
#import "FPPopoverController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "Tabla1.h"
#import "Tabla2.h"
#import "Tabla3.h"


@interface MiPropiedadViewController ()

@end

@implementation MiPropiedadViewController
@synthesize boton, boton2,boton3, searchView, estadosSearch, estadosPicker, deseaSearch, tipoSearch;
@synthesize  scroll;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
    
    [self JSONrequest];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additioal setup after loading the view.
    NSArray *array = [[NSArray alloc]initWithObjects:@"Venta", @"Renta",@"Venta/renta",
                      @"tiempo compartido",
                      @"aportación",
                      @"permuta total",
                      @"permuta parcial",
                      @"traspaso",
                      @"atraque",
                      @"otro", nil];
    self.deseaSearch = array;
    
    NSArray *array1 = [[NSArray alloc]initWithObjects:@"Bodega",
                       @"Bodega en Condominio",
                       @"Casa en Condominio",
                       @"Casa sola",
                       @"Departamento en Condominio",
                       @"Dúplex",
                       @"Edificio",
                       @"Escuela",
                       @"Fábrica",
                       @"Fraccionamiento",
                       @"Huerta",
                       @"Inmueble Productivo Urbano",
                       @"Local",
                       @"Nave",
                       @"Nave Industrial",
                       @"Oficina",
                       @"Oficina en Condominio",
                       @"Penthouse",
                       @"Quinta",
                       @"Rancho",
                       @"Suite"
                       @"Terreno", nil];
    
    self.tipoSearch = array1;
                
    
    scrollCount = 0;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *butImage = [[UIImage imageNamed:@"boton-regresar.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    [button setBackgroundImage:butImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    // [button setTitle:@"Back" forState:UIControlStateNormal];
    //  button.titleLabel.text = @"Back";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;

    ///
    UITapGestureRecognizer *recognizer;
    
    recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmissKeyB:)];
    [recognizer setNumberOfTapsRequired:1];
    [self.scroll addGestureRecognizer:recognizer];
 
    ////
    self.nombre.delegate = self;
    self.email.delegate = self;
    self.telefono.delegate = self;
    self.calle.delegate = self;
    self.noExt.delegate = self;
    self.noInt.delegate = self;
    self.colonia.delegate = self;
    
    self.CP.delegate = self;
    self.ciudad.delegate = self;
    self.m2construccion.delegate = self;
    self.m2terreno.delegate = self;
    self.recamaras.delegate = self;
    self.banos.delegate = self;
    self.antiguedad.delegate = self;
    self.estacionamientos.delegate = self;
    self.precioEst.delegate = self;
    
    self.comentarios.delegate = self;
    
   // self.precioEst.delegate = self;
    
    //
    
    scroll.scrollEnabled = YES;
    [scroll setContentSize:CGSizeMake(320, 416)];
    scroll.delegate=self;
    
    self.scroll.backgroundColor = [UIColor blackColor];
}

- (IBAction) dissmissKeyB: (id) sender{

    [self.nombre resignFirstResponder];
    [self.email resignFirstResponder];
    [self.telefono resignFirstResponder];
    [self.calle resignFirstResponder];
    [self.noInt resignFirstResponder];
    [self.noExt resignFirstResponder];
    [self.colonia resignFirstResponder];
    
        [self.CP resignFirstResponder];
        [self.ciudad resignFirstResponder];
        [self.m2construccion resignFirstResponder];
        [self.m2terreno resignFirstResponder];
        [self.recamaras resignFirstResponder];
        [self.banos resignFirstResponder];
        [self.antiguedad resignFirstResponder];
        [self.estacionamientos resignFirstResponder];
        [self.precioEst resignFirstResponder];
    [self.comentarios resignFirstResponder];
    
    if (scrollCount == 1) {
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.3];
        
        
        
        CGRect scrollFrame = scroll.frame;
        //	scrollFrame.origin.y += 150;
        // scrollFrame.origin.x += 150;
        scrollFrame.size.height +=200;
        scroll.frame = scrollFrame;
        
        [UIView commitAnimations];
        
        scrollCount = 0;
        
    }

}

- (IBAction) tapBackground: (id) sender{
	[self resignFirstResponder];
    
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)Click:(id)sender
{
    Tabla1 *t1 = [[Tabla1 alloc] initWithStyle:UITableViewStyleGrouped];
    t1.title = [NSString stringWithFormat:@"Opciones"];
    t1.delegate = self;
    popover = [[FPPopoverController alloc] initWithViewController:t1];
    [popover presentPopoverFromView:sender];
    
    
    
}

-(void)selectedTableRow:(NSUInteger)rowNum
{
    [popover dismissPopoverAnimated:YES];
    
}

-(IBAction)Click2:(id)sender
{
    Tabla2 *t2 = [[Tabla2 alloc] initWithStyle:UITableViewStyleGrouped];
    t2.title = [NSString stringWithFormat:@"Inmuebles"];
    t2.delegate = self;
    popover2 = [[FPPopoverController alloc] initWithViewController:t2];
    [popover2 presentPopoverFromView:sender];
}
-(void)selectedTableRow2:(NSUInteger)rowNum
{
    [popover2 dismissPopoverAnimated:YES];
    
}


-(IBAction)Click3:(id)sender
{
    Tabla3 *t3 = [[Tabla3 alloc] initWithStyle:UITableViewStyleGrouped];
    t3.title = [NSString stringWithFormat:@"Estados"];
    t3.delegate = self;
    popover3 = [[FPPopoverController alloc] initWithViewController:t3];
    [popover3 presentPopoverFromView:sender];
}
-(void)selectedTableRow3:(NSUInteger)rowNum
{
    [popover3 dismissPopoverAnimated:YES];
    
}

-(IBAction)gotoBack:(id)sender
{
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"Ultimo-header-pagina-principal.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{

    NSLog(@"textFieldDidBeginEditing");
    
    if (scrollCount == 0) {
        
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.3];
    
    
	
	CGRect scrollFrame = scroll.frame;
    //	scrollFrame.origin.y += 150;
    // scrollFrame.origin.x += 150;
    scrollFrame.size.height -=200;
	scroll.frame = scrollFrame;
	
	[UIView commitAnimations];
    
     [self becomeFirstResponder];
        scrollCount = 1;
    }

}
-(void)textViewDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldDidBeginEditing");
    
    if (scrollCount == 0) {
        
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.3];
        
        
        
        CGRect scrollFrame = scroll.frame;
        //	scrollFrame.origin.y += 150;
        // scrollFrame.origin.x += 150;
        scrollFrame.size.height -=200;
        scroll.frame = scrollFrame;
        
        [UIView commitAnimations];
        
        [self becomeFirstResponder];
        scrollCount = 1;
    }
    
}

-(IBAction)enviar{
    NSString *selection= [NSString stringWithFormat:@"%@",self.boton.titleLabel.text];
    NSString *selection2= [NSString stringWithFormat:@"%@",self.boton2.titleLabel.text];
    NSString *selection3= [NSString stringWithFormat:@"%@",self.boton3.titleLabel.text];
                          
    NSString *body = [NSString stringWithFormat:@"Nombre:%@, email:%@, telefono:%@, calle: %@, No.Exterior: %@, No. Interior, %@, Colonia: %@, CP: %@, Ciudad: %@, M2 construcción: %@, M2 terreno: %@, Recamaras: %@, Baños: %@, Antiguedad: %@, Estacionemientos: %@, Precio estimado: %@, Proposito: %@, Tipo: %@, Estado: %@", self.nombre.text, self.email.text, self.telefono.text, self.calle.text, self.noExt.text, self.noInt.text, self.colonia.text, self.CP.text, self.ciudad.text, self.m2construccion.text, self.m2terreno.text, self.recamaras.text, self.banos.text, self.antiguedad.text, self.estacionamientos.text, self.precioEst.text, selection, selection2, selection3];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    NSArray *recipients = [[NSArray alloc]initWithObjects:@"Correo", nil];
    [picker setToRecipients:recipients];
    [picker setSubject:[NSString stringWithFormat:@"Ofresco propiedad"]];
    [picker setMessageBody:[NSString stringWithFormat:@"%@", body] isHTML:YES];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    [self dissmissKeyB:nil];

}
-(IBAction)borrar{
    
    self.nombre.text = @"";
    self.email.text = @"";
    self.telefono.text = @"";
    self.calle.text = @"";
    self.noExt.text = @"";
    self.noInt.text = @"";
    self.colonia.text = @"";
    
    self.CP.text = @"";
    self.ciudad.text = @"";
    self.m2construccion.text = @"";
    self.m2terreno.text = @"";
    self.recamaras.text = @"";
    self.banos.text = @"";
    self.antiguedad.text = @"";
    self.estacionamientos.text = @"";
    self.precioEst.text = @"";
    self.comentarios.text = @"";

    [self dissmissKeyB:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)JSONrequest {
    
    NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat: @"http://century21.elolabs.com:8888/update_colonias"]];
    
    NSData* data = [NSData dataWithContentsOfURL:
                    url];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data waitUntilDone:YES];
    
}

- (void)fetchedData:(NSData *)responseData {
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSDictionary* EstadosDict = [json objectForKey:@"estados"]; //2
    NSArray* estadosA = [[json objectForKey:@"estados"] allKeys];
    
    self.estadosSearch = estadosA;
}

-(IBAction)pushPicker{
    
   
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    searchView.alpha = 1.0;
	CGRect proFrame = searchView.frame;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            proFrame.origin.y -= 172;
        }else if (iOSDeviceScreenSize.height == 568){
            proFrame.origin.y -= 260;
        }
    }

    
	//proFrame.origin.y -= 260;
	searchView.frame = proFrame;
    
	[UIView commitAnimations];
    
}

-(void)returnPicker{
    
    self.boton.enabled = YES;
    self.boton2.enabled = YES;
    self.boton3.enabled = YES;

    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    searchView.alpha = 1.0;
	CGRect proFrame = searchView.frame;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            proFrame.origin.y += 172;
        }else if (iOSDeviceScreenSize.height == 568){
            proFrame.origin.y += 260;
        }
    }
	
    //proFrame.origin.y += 260;
	searchView.frame = proFrame;
    
	[UIView commitAnimations];
    
}

-(IBAction)estados{
    
    self.boton.enabled = NO;
    self.boton2.enabled = NO;
    
    searchControl = 2;
    
    [self.estadosPicker reloadAllComponents];
    [self.estadosPicker reloadInputViews];
    
  //  if (searchControl == 2) {
        
   // }
    
    [self pushPicker];

}
-(IBAction)tipo{
    self.boton.enabled = NO;
    self.boton3.enabled = NO;
    
    searchControl = 1;
    
    [self.estadosPicker reloadAllComponents];
    [self.estadosPicker reloadInputViews];
    
    [self pushPicker];

}
-(IBAction)desea{
    
    self.boton3.enabled = NO;
    self.boton2.enabled = NO;

    searchControl = 0;
    
    [self.estadosPicker reloadAllComponents];
    [self.estadosPicker reloadInputViews];
    
    [self pushPicker];
}

-(IBAction)search{
    
     NSInteger row;
    row = [self.estadosPicker selectedRowInComponent:0];
    
     
    
    if (searchControl==0) {
       NSString *selection= [NSString stringWithFormat:@"%@",[self.deseaSearch objectAtIndex:row] ];
        boton.titleLabel.text = selection;
        
    }else if (searchControl==1){
        NSString *selection= [NSString stringWithFormat:@"%@",[self.tipoSearch objectAtIndex:row] ];
         boton2.titleLabel.text = selection;
    
    }else if (searchControl==2){
        NSString *selection= [NSString stringWithFormat:@"%@",[self.estadosSearch objectAtIndex:row] ];
         boton3.titleLabel.text = selection;
    
    }
[self returnPicker];


}
#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (searchControl == 0) {
        return [self.deseaSearch count];
    }
    else if (searchControl == 1) {
        return [self.tipoSearch count];
    }else{
        return [self.estadosSearch count];
    }
}
#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (searchControl == 0) {
        return [self.deseaSearch objectAtIndex:row];
    }
    else if (searchControl == 1) {
        return [self.tipoSearch objectAtIndex:row];
    }else{
        return [self.estadosSearch objectAtIndex:row];
    }
    
}



@end
