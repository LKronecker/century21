//
//  Tabla1.m
//  Century21
//
//  Created by Alan MB on 26/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//



#import "Tabla1.h"
#import "MiPropiedadViewController.h"

@interface Tabla1 ()
{
    NSMutableArray *menu;

}

@end

@implementation Tabla1
@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {}
        
           
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

   
    menu = [[NSMutableArray alloc]init];
    
    [menu addObject:@"Opcion 1"];
    [menu addObject:@"Opcion 2"];
    [menu addObject:@"Opcion 3"];
    [menu addObject:@"Opcion 4"];


    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [menu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }

    
    // Configure the cell...
    
  
   cell.textLabel.text = [menu objectAtIndex:indexPath.row];
        
    
    if (indexPath.row == SelectedIndex) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }


    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SelectedIndex = [indexPath row];
    NSString *text = [menu objectAtIndex:SelectedIndex];
   

    [_delegate.boton setTitle:text forState:UIControlStateNormal];
    
    
    [self.delegate selectedTableRow:indexPath.row];

     [tableView reloadData];
       
}

@end
